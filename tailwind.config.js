const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors')

module.exports = {
    mode: 'jit',
    purge: [
        './resources/views/!(dashboard)/**/*.blade.php',
        './resources/js/!(dashboard)/**/*.vue',
    ],
    darkMode: false,
    theme: {
        extend: {
            width: {
                '128': '32rem'
            },
            height: {
                '128': '32rem'
            },
            fontFamily: {
                roboto: [
                    'system-ui',
                    '-apple-system', /* Firefox supports this but not yet `system-ui` */
                    'Roboto',
                    'Segoe UI',
                    'Helvetica',
                    'Arial',
                    'sans-serif',
                    'Apple Color Emoji',
                    'Segoe UI Emoji'
                ],
            }
        }
    },
    variants: {
        extend: {
            textOpacity: ['dark'],
            backgroundOpacity: ['dark'],
            borderOpacity: ['dark'],
        },
    },

    plugins: [require('@tailwindcss/forms')],
};
