<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAudioVideoFieldsToAssetVersions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_versions', function (Blueprint $table) {
            $table->foreignId('video_id')->nullable()->after('image_id');
            $table->foreignId('audio_id')->nullable()->after('video_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_versions', function (Blueprint $table) {
            $table->dropColumn('video_id');
            $table->dropColumn('audio_id');
        });
    }
}
