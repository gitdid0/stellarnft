<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAssetVersionIdFieldToSearchables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('searchables', function (Blueprint $table) {
            $table->foreignId('asset_version_id')->default(0)->after('name');
            $table->index('asset_version_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('searchables', function (Blueprint $table) {
            $table->dropColumn('asset_version_id');
        });
    }
}
