<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_versions', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('issuer');
            $table->boolean('tradable')->default(0);
            $table->bigInteger('price');
            $table->bigInteger('min_offer');
            $table->string('name');
            $table->text('description')->nullable();
            $table->foreignId('image_id')->nullable();
            $table->timestamps();
            $table->index(['code', 'issuer']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_versions');
    }
}
