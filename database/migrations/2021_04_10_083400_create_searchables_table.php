<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSearchablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('searchables', function (Blueprint $table) {
            $table->id();
            $table->foreignId('asset_id')
                ->references('id')
                ->on('assets')
                ->onDelete('cascade');
            $table->string('code', 12);
            $table->string('issuer', 64);
            $table->string('owner', 64)->nullable();
            $table->boolean('active')->default(0);
            $table->boolean('banned')->default(0);
            $table->boolean('has_offers')->default(0);
            $table->boolean('nsfw')->default(0);
            $table->boolean('private')->default(0);
            $table->bigInteger('price');
            $table->string('name');
            $table->dateTime('sold_at')->nullable();
            $table->timestamps();
            $table->index(['asset_id', 'code', 'issuer', 'name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('searchables');
    }
}
