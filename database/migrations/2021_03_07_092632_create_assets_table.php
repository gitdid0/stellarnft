<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->id();
            $table->foreignId('asset_version_id');
            $table->string('code');
            $table->string('issuer');
            $table->string('owner')->nullable();
            $table->string('escrow_public_key');
            $table->string('escrow_secret_key');
            $table->boolean('active')->default(0);
            $table->boolean('banned')->default(0);
            $table->json('dataentries')->nullable();
            $table->string('home_domain')->nullable();
            $table->timestamps();
            $table->index(['code', 'issuer']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
