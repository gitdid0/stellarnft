from flask import Flask, request, json
from check_auth_transaction import AuthTransaction
from get_keypair import GetKeypair
from get_xdr import GetXDR
from accept_offer import AcceptOffer
from check_offer_xdr import CheckOfferXDR
from delete_asset import DeleteAsset
from force_delete_asset import ForceDeleteAsset

app = Flask(__name__)

@app.route('/check-auth-transaction', methods=['POST'])
def checkAuthTransaction():

    transaction = (
        AuthTransaction(
            xdr=request.json['xdr'],
            sessionCode=request.json['sessionCode'],
            publicKey=request.json['publicKey'],
            app=app
        )
    )
    response = app.response_class(
        response=json.dumps(transaction.validate()),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/get-keypair', methods=['POST'])
def getKeypairPost():

    response = app.response_class(
        response=json.dumps(GetKeypair.generate()),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/get-xdr', methods=['POST'])
def getXDR():

    xdrBuilder = (
        GetXDR(
            signer=request.json['signer'],
            operations=request.json['operations'],
            test=request.json['test'],
            app=app
        )
    )

    response = app.response_class(
        response=json.dumps(xdrBuilder.generate()),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/accept-offer', methods=['POST'])
def acceptOffer():

    offer = (
        AcceptOffer(
            signers=request.json['signers'],
            signed_xdr=request.json['signed_xdr'],
            xdr=request.json['xdr'],
            horizon=request.json['horizon'],
            site_secret=request.json['site_secret'],
            escrow_secret=request.json['escrow_secret'],
            test=request.json['test'],
            app=app
        )
    )

    response = app.response_class(
        response=json.dumps(offer.accept()),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/delete-asset', methods=['POST'])
def deleteAsset():

    asset = (
        DeleteAsset(
            owner=request.json['owner'],
            xdr=request.json['xdr'],
            horizon=request.json['horizon'],
            site_secret=request.json['site_secret'],
            escrow_secret=request.json['escrow_secret'],
            test=request.json['test'],
            app=app
        )
    )

    response = app.response_class(
        response=json.dumps(asset.delete()),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/force-delete-asset', methods=['POST'])
def forceDeleteAsset():

    asset = (
        ForceDeleteAsset(
            owner=request.json['owner'],
            horizon=request.json['horizon'],
            site_secret=request.json['site_secret'],
            escrow_secret=request.json['escrow_secret'],
            test=request.json['test'],
            app=app
        )
    )

    response = app.response_class(
        response=json.dumps(asset.delete()),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/check-offer-xdr', methods=['POST'])
def checkOfferXDR():

    transaction = (
        CheckOfferXDR(
            signers=request.json['signers'],
            xdr=request.json['xdr'],
            transaction_hash=request.json['hash'],
            test=request.json['test'],
            app=app
        )
    )
    response = app.response_class(
        response=json.dumps(transaction.validate()),
        status=200,
        mimetype='application/json'
    )
    return response

if __name__ == "__main__":
    from waitress import serve
    serve(app, host="127.0.0.1", port=5000)