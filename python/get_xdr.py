from datetime import datetime
from stellar_sdk import Keypair, Network, helpers, Asset, Claimant, ClaimPredicate, TransactionBuilder, Account
from stellar_sdk.exceptions import BadSignatureError

class GetXDR:
    def __init__(self, signer, operations, test, app):
        self.signer = signer
        self.operations = operations
        self.test = test
        self.app = app

    def generate(self):

        source_keypair = Keypair.from_secret(self.signer['secret'])

        source_account = Account(account_id=source_keypair.public_key, sequence=int(self.signer['sequence']))

        passphrase = Network.PUBLIC_NETWORK_PASSPHRASE

        if self.test:
            passphrase = Network.TESTNET_NETWORK_PASSPHRASE

        transaction = (
            TransactionBuilder(
                source_account=source_account,
                network_passphrase=passphrase,
                base_fee=100,
            )
        )

        for i in range(len(self.operations)):
            operation = self.operations[i]

            if operation['type'] == 'memo':
                transaction = transaction.add_text_memo(operation['text'])

            if operation['type'] == 'claimable_balance' and operation['asset_code'] == 'XLM':
                claimants = []
                claimants.append(Claimant(destination=operation['destination']))
                if operation['relative_time'] :
                    predicate = ClaimPredicate.predicate_not(ClaimPredicate.predicate_before_relative_time(seconds=int(operation['relative_time'])))
                    claimants.append(Claimant(destination=operation['source'], predicate=predicate))
                transaction = (
                    transaction.append_create_claimable_balance_op(
                        asset=Asset.native(),
                        amount=operation['amount'],
                        claimants=claimants,
                        source=operation['source']
                    )
                )
            
            if operation['type'] == 'claimable_balance' and operation['asset_code'] != 'XLM':
                claimants = []
                claimants.append(Claimant(destination=operation['destination']))
                if operation['relative_time'] :
                    predicate = ClaimPredicate.predicate_not(ClaimPredicate.predicate_before_relative_time(seconds=int(operation['relative_time'])))
                    claimants.append(Claimant(destination=operation['source'], predicate=predicate))
                transaction = (
                    transaction.append_create_claimable_balance_op(
                        asset=Asset(code=operation['asset_code'], issuer=operation['asset_issuer']),
                        amount=operation['amount'],
                        claimants=claimants,
                        source=operation['source']
                    )
                )

            if operation['type'] == 'payment' and operation['asset_code'] == 'XLM':
                transaction = (
                    transaction.append_payment_op(
                        destination=operation['destination'],
                        amount=operation['amount'],
                        asset_code=operation['asset_code'],
                        source=operation['source']
                    )
                )

            if operation['type'] == 'payment' and operation['asset_code'] != 'XLM':
                transaction = (
                    transaction.append_payment_op(
                        destination=operation['destination'],
                        amount=operation['amount'],
                        asset_code=operation['asset_code'],
                        asset_issuer=operation['asset_issuer'],
                        source=operation['source']
                    )
                )
            
            if operation['type'] == 'change_trust':
                transaction = (
                    transaction.append_change_trust_op(
                        asset_code=operation['asset_code'],
                        asset_issuer=operation['asset_issuer'],
                        limit=operation['limit'],
                        source=operation['source']
                    )
                )

        transaction = transaction.build()

        #transaction.sign(source_keypair)
            
        xdr = transaction.to_xdr()
        tx_hash = transaction.hash_hex()

        output = {}
        output["xdr"] = xdr
        output["hash"] = tx_hash

        return output