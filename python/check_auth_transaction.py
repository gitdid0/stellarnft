import time, base64

from datetime import datetime
from stellar_sdk import Keypair, ManageData, Network, helpers
from stellar_sdk.exceptions import BadSignatureError

class AuthTransaction:
    def __init__(self, xdr, sessionCode, publicKey, app):
        self.xdr = xdr
        self.sessionCode = sessionCode
        self.publicKey = publicKey
        self.app = app

    def validate(self):

        try:
            transactionEnvelop = helpers.parse_transaction_envelope_from_xdr(self.xdr, Network.PUBLIC_NETWORK_PASSPHRASE)
        except:
            return False

        transaction = transactionEnvelop.transaction

        keypair = transaction.source

        tx_hash = transactionEnvelop.hash()

        if(transaction.sequence != 1):
            self.app.logger.warning('Sequence Error')
            return False

        if not transaction.time_bounds:
            return False

        max_time = transaction.time_bounds.max_time

        if max_time == 0:
            self.app.logger.warning('Max Time Error')
            return False

        current_time = datetime.timestamp(datetime.utcnow())

        if current_time > max_time:
            self.app.logger.warning('Time Error')
            return False

        if len(transaction.operations) < 1:
            self.app.logger.warning('Operations Error')
            return False

        manage_data_op = transaction.operations[0]
        if not isinstance(manage_data_op, ManageData):
            self.app.logger.warning('Not ManageData Error')
            return False

        client_account = manage_data_op.source
        if not client_account:
            self.app.logger.warning('No ManageData Source Error')
            return False
        if client_account != keypair.public_key:
            self.app.logger.warning('ManageData Source Not Match Source Error')
            return False

        if manage_data_op.data_value is None:
            self.app.logger.warning('ManageData data none')
            return False

        if manage_data_op.data_name != self.sessionCode:
            self.app.logger.warning('ManageData data not session code')
            return False

        signer_found = False
        signers_found = []
        signature_used = set()

        for index, decorated_signature in enumerate(transactionEnvelop.signatures):
            if index in signature_used:
                continue
            if decorated_signature.hint.signature_hint != keypair.signature_hint():
                continue
            try:
                keypair.verify(tx_hash, decorated_signature.signature.signature)
                if(keypair.public_key == self.publicKey):
                    signer_found = True
                    signature_used.add(index)
                    signers_found.append(keypair.public_key)
                break
            except BadSignatureError:
                pass

        if signer_found:
            return True
        else:
            self.app.logger.warning('No proper signers')
            return False