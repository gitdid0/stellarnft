import time, base64

from datetime import datetime
from stellar_sdk import Keypair, ChangeTrust, Payment, Network, helpers
from stellar_sdk.exceptions import BadSignatureError

class CheckOfferXDR:
    def __init__(self, signers, xdr, transaction_hash, test, app):
        self.signers = signers
        self.xdr = xdr
        self.transaction_hash = transaction_hash
        self.test = test
        self.app = app

    def validate(self):

        passphrase = Network.PUBLIC_NETWORK_PASSPHRASE

        if self.test:
            passphrase = Network.TESTNET_NETWORK_PASSPHRASE

        try:
            transactionEnvelop = helpers.parse_transaction_envelope_from_xdr(self.xdr, passphrase)
        except:
            self.app.logger.warning('Envelop Error')
            return False

        transaction = transactionEnvelop.transaction

        tx_hash = transactionEnvelop.hash()

        str_tx_hash = str(transactionEnvelop.hash_hex())

        if str_tx_hash != self.transaction_hash:
            return False

        for i in range(len(self.signers)):
            signer_public_key = self.signers[i]
            signer_keypair = Keypair.from_public_key(signer_public_key)
            signer_found = False
            signature_used = set()
            for index, decorated_signature in enumerate(transactionEnvelop.signatures):
                if index in signature_used:
                    continue
                try:
                    signer_keypair.verify(tx_hash, decorated_signature.signature.signature)
                    signer_found = True
                    signature_used.add(index)
                    break
                except BadSignatureError:
                    pass

            if signer_found != True:
                self.app.logger.warning('No signer ' + signer_public_key)
                return False

        return True