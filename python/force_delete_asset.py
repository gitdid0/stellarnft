from datetime import datetime
from stellar_sdk import Keypair, Network, helpers, Asset, Claimant, TransactionBuilder, Account, Server, AccountMerge
from stellar_sdk.exceptions import BadSignatureError

class ForceDeleteAsset:
    def __init__(self, owner, horizon, site_secret, escrow_secret, test, app):
        self.owner = owner
        self.horizon = horizon
        self.site_secret = site_secret
        self.escrow_secret = escrow_secret
        self.test = test
        self.app = app

    def delete(self):

        passphrase = Network.PUBLIC_NETWORK_PASSPHRASE

        if self.test:
            passphrase = Network.TESTNET_NETWORK_PASSPHRASE

        site_keypair = Keypair.from_secret(self.site_secret)

        server = Server(horizon_url=self.horizon)

        escrow_keypair = Keypair.from_secret(self.escrow_secret)

        source_account = server.load_account(site_keypair.public_key)

        transaction = (
            TransactionBuilder(
                source_account=source_account,
                network_passphrase=passphrase,
                base_fee=100,
            )
        )
        transaction = transaction.append_account_merge_op(
            destination=self.owner,
            source=escrow_keypair.public_key
        )
        transaction = transaction.build()

        transaction.sign(escrow_keypair)
        transaction.sign(site_keypair)

        response = server.submit_transaction(transaction)

        return response