import time, base64

from stellar_sdk import Keypair

class GetKeypair:

    def generate():

        keypair = Keypair.random()

        output = {}
        output["public"] = keypair.public_key
        output["secret"] = keypair.secret

        return output