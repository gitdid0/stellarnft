from datetime import datetime
from stellar_sdk import Keypair, Network, helpers, Asset, Claimant, TransactionBuilder, Account, Server
from stellar_sdk.exceptions import BadSignatureError

class AcceptOffer:
    def __init__(self, signers, signed_xdr, xdr, horizon, site_secret, escrow_secret, test, app):
        self.signers = signers
        self.signed_xdr = signed_xdr
        self.xdr = xdr
        self.horizon = horizon
        self.site_secret = site_secret
        self.escrow_secret = escrow_secret
        self.test = test
        self.app = app

    def accept(self):

        passphrase = Network.PUBLIC_NETWORK_PASSPHRASE

        if self.test:
            passphrase = Network.TESTNET_NETWORK_PASSPHRASE

        unsigned_transaction = helpers.parse_transaction_envelope_from_xdr(self.xdr, passphrase)

        unsigned_tx_hash = unsigned_transaction.hash_hex()

        signed_transaction = helpers.parse_transaction_envelope_from_xdr(self.signed_xdr, passphrase)

        signed_tx_hash = signed_transaction.hash_hex()

        if unsigned_tx_hash != signed_tx_hash:
            return False

        tx_hash = signed_transaction.hash()

        for i in range(len(self.signers)):
            signer_public_key = self.signers[i]
            signer_keypair = Keypair.from_public_key(signer_public_key)
            signer_found = False
            signature_used = set()
            for index, decorated_signature in enumerate(signed_transaction.signatures):
                if index in signature_used:
                    continue
                try:
                    signer_keypair.verify(tx_hash, decorated_signature.signature.signature)
                    signer_found = True
                    signature_used.add(index)
                    break
                except BadSignatureError:
                    pass

            if signer_found != True:
                self.app.logger.warning('No signer ' + signer_public_key)
                return False

        escrow_keypair = Keypair.from_secret(self.escrow_secret)

        escrow_signed = False
        signature_used = set()
        for index, decorated_signature in enumerate(signed_transaction.signatures):
            if index in signature_used:
                continue
            try:
                escrow_keypair.verify(tx_hash, decorated_signature.signature.signature)
                escrow_signed = True
                signature_used.add(index)
                break
            except BadSignatureError:
                pass

        if escrow_signed != True:
            signed_transaction.sign(escrow_keypair)

        site_keypair = Keypair.from_secret(self.site_secret)

        fee_bump_tx = TransactionBuilder.build_fee_bump_transaction(
            fee_source=site_keypair,
            base_fee=200,
            inner_transaction_envelope=signed_transaction, network_passphrase=passphrase
        )
        fee_bump_tx.sign(site_keypair)

        server = Server(horizon_url=self.horizon)

        response = server.submit_transaction(fee_bump_tx)

        return response