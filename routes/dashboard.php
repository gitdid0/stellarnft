<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard\AssetController;
use App\Http\Controllers\Dashboard\OfferController;
use App\Http\Controllers\Dashboard\ReportController;
use App\Http\Controllers\Dashboard\SettingController;
use Rap2hpoutre\LaravelLogViewer\LogViewerController;
use App\Http\Controllers\Dashboard\ActivityController;
use App\Http\Controllers\Dashboard\CategoryController;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\Dashboard\VerifiedAddressController;
use App\Http\Controllers\Dashboard\ForbiddenAddressController;

Route::prefix('dashboard')->middleware(['web', 'admin'])->name('dashboard.')->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('index');
    Route::resource('/assets', AssetController::class);
    Route::resource('/offers', OfferController::class);
    Route::resource('/forbidden-addresses', ForbiddenAddressController::class);
    Route::resource('/verified-addresses', VerifiedAddressController::class);
    Route::resource('/activities', ActivityController::class);
    Route::resource('/reports', ReportController::class);
    Route::resource('/settings', SettingController::class);
    Route::resource('/categories', CategoryController::class);
    Route::get('logs', [LogViewerController::class, 'index'])->name('logs');
});