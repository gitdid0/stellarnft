<?php

namespace App\Activities;

use App\Models\Activity as Model;
use App\Helpers\ActivityHelper;

class Activity
{
    public static function getName()
    {
        return __('Activity');
    }

    public static function getInfoTemplate()
    {
        return '';
    }

    public static function addActivityToDB($type, $assetId, $data)
    {
        $activity = new Model;
        $activity->type = $type;
        $activity->data = $data;
        $activity->asset_id = $assetId;
        $activity->save();

        ActivityHelper::clearCache($assetId);
        
        return true;
    }
}