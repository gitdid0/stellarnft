<?php

namespace App\Activities;

use App\Helpers\ActivityHelper;
use App\Jobs\Asset\GetNewOwner;
use App\Models\Activity as Model;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;

class Created extends Activity
{
    public static $type = '\App\Activities\Created';

    public static function getName()
    {
        return __('Created');
    }

    public static function getInfoTemplate()
    {
        return 'assets.activities.created';
    }

    public static function addActivity($assetId, $data)
    {
        return self::addActivityToDB(self::$type, $assetId, $data);
    }
}