<?php

namespace App\Activities;

use App\Jobs\Asset\GetNewOwner;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;

class Sale extends Activity
{
    public static $type = '\App\Activities\Sale';

    public static function getName()
    {
        return __('Sale');
    }

    public static function getInfoTemplate()
    {
        return 'assets.activities.sale';
    }

    public static function addActivity($assetId, $data)
    {
        return self::addActivityToDB(self::$type, $assetId, $data);
    }
}