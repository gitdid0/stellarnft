<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'trade' => 'boolean'
    ];

    public function offer_asset()
    {
        return $this->belongsTo('App\Models\Asset', 'asset_id', 'id');
    }

    public function getLabUrlAttribute()
    {
        return 'https://laboratory.stellar.org/#xdr-viewer?input='.urlencode($this->xdr).'&type=TransactionEnvelope&network=public';
    }
}
