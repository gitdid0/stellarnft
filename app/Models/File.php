<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class File extends Model
{
    use HasFactory;

    protected $guarded = [];

    public $appends = ['url'];

    protected $casts = [
        'thumbnail' => 'array',
    ];

    public function getLocalUrlAttribute()
    {
        if(!empty($this->path)) {
            return Storage::disk('local')->url($this->path);
        }

        return '';
    }

    public function getCloudUrlAttribute()
    {
        if(!empty($this->object_path)) {
            return config('site.static_files_host').$this->object_path;
        }

        return '';
    }

    public function getUrlAttribute()
    {
        if(!empty($this->object_path)) {
            return config('site.static_files_host').$this->object_path;
        }

        return Storage::disk('local')->url($this->path);
    }

    public function thumb($size)
    {
        if(!empty($this->object_path)) {
            $url = config('site.static_files_host').$this->object_path;
            if(!empty($this->thumbnail) && isset($this->thumbnail[$size]) && strpos($url, '.gif') === false) {
                $url = config('site.static_files_host').$this->thumbnail[$size];
            }

            return $url;
        }

        return Storage::disk('local')->url($this->path);
    }
}
