<?php

namespace App\Console\Commands\File;

use DirectoryIterator;
use Illuminate\Console\Command;

class ClearTmp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'file:clear-tmp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear tmp files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $folderName = config('site.tmp_folder');
        if (file_exists($folderName)) {
            foreach (new DirectoryIterator($folderName) as $fileInfo) {
                if ($fileInfo->isDot()) {
                    continue;
                }
                if ($fileInfo->isFile() && time() - $fileInfo->getCTime() >= 24*60*60) {
                    unlink($fileInfo->getRealPath());
                }
            }
        }
    }
}
