<?php

namespace App\Jobs;

use App\Helpers\AssetHelper;
use App\Models\File;
use App\Models\AssetVersion;
use Illuminate\Bus\Queueable;
use App\Helpers\FileHelper;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class MoveFileToObjectStorage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(File $file)
    {
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file = $this->file;

        FileHelper::moveFileToObjectStorage($file->id);

        $assetVersion = AssetVersion::where('image_id', $file->id)->orWhere('video_id', $file->id)->orWhere('audio_id', $file->id)->first();
        if($assetVersion) {
            AssetHelper::clearAssetCache($assetVersion->code, $assetVersion->issuer);
        }
    }
}
