<?php

namespace App\Jobs\Asset;

use App\Models\Offer;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ClearOffers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $asset_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($asset_id)
    {
        $this->asset_id = $asset_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $asset_id = $this->asset_id;

        Offer::where('asset_id', $asset_id)->delete();
    }
}