<?php

namespace App\Jobs\Asset;

use App\Models\Asset;
use App\Helpers\NFTHelper;
use App\Helpers\AssetHelper;
use Illuminate\Bus\Queueable;
use App\Jobs\Asset\ClearPrevOffers;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class GetNewOwner implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $asset;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Asset $asset)
    {
        $this->asset = $asset;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $asset = $this->asset;
        $newOwner = NFTHelper::getNFTOwner($asset->code, $asset->issuer);
        if($newOwner) {
            $asset->owner = $newOwner;
            $asset->save();
            AssetHelper::clearAssetCache($asset->code, $asset->issuer);

            ClearPrevOffers::dispatch($asset);
        }
    }

    public function uniqueId()
    {
        return $this->asset->id;
    }
}
