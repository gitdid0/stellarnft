<?php

namespace App\Jobs\Asset;

use App\Models\Asset;
use App\Helpers\AssetHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ForceDelete implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $asset;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Asset $asset)
    {
        $this->asset = $asset;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $asset = $this->asset;

        $tx = AssetHelper::forceDeleteAsset($asset);

        $assetId = $asset->id;
        $code = $asset->code;
        $issuer = $asset->issuer;

        $asset->delete();

        AssetHelper::clearAssetCache($code, $issuer);

        ClearOffers::dispatch($assetId);

        DeleteVersions::dispatch($code, $issuer);
    }
}
