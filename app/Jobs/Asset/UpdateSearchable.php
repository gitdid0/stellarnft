<?php

namespace App\Jobs\Asset;

use App\Models\Searchable;
use App\Helpers\AssetHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class UpdateSearchable implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $asset;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($asset)
    {
        $this->asset = $asset;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $asset = $this->asset;

        $searchData = AssetHelper::getSearchData($asset);
        $searchable = Searchable::updateOrCreate(
            ['asset_id' => $asset->id],
            $searchData
        );
    }

    public function uniqueId()
    {
        return $this->asset->id;
    }
}
