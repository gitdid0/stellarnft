<?php

namespace App\Jobs\Asset;

use App\Models\Asset;
use App\Helpers\AssetHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Queue\Middleware\ThrottlesExceptionsWithRedis;

class CheckActive implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $asset;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Asset $asset)
    {
        $this->asset = $asset;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        sleep(2);

        $asset = $this->asset;

        $horizonServer = config('site.horizonServer');

        $response = Http::get($horizonServer. 'accounts/' . $asset->escrow_public_key);

        if($response->ok()) {
            $account = $response->json();
            if(!empty($account['sequence'])) {
                $asset->active = true;
                $asset->save();
                AssetHelper::clearAssetCache($asset->code, $asset->issuer);
            }
        }

        sleep(2);

        Cache::forget('check-active-'.$asset->id);
    }
}
