<?php

namespace App\Jobs\Asset;

use App\Helpers\AssetHelper;
use App\Models\AssetVersion;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class DeleteVersions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $code;

    public $issuer;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($code, $issuer)
    {
        $this->code = $code;
        $this->issuer = $issuer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $code = $this->code;

        $issuer = $this->issuer;

        $assetVersions = AssetVersion::where('code', $code)->where('issuer', $issuer)->get();
        foreach($assetVersions as $assetVersion) {
            AssetHelper::deleteAssetVersion($assetVersion);
        }
    }
}
