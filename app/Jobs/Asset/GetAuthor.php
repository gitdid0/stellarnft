<?php

namespace App\Jobs\Asset;

use App\Models\Asset;
use App\Helpers\NFTHelper;
use App\Helpers\AssetHelper;
use Illuminate\Bus\Queueable;
use App\Helpers\MarketplaceHelper;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class GetAuthor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $asset;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Asset $asset)
    {
        $this->asset = $asset;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $asset = $this->asset;
        $author = NFTHelper::getNFTAuthor($asset->code, $asset->issuer);
        if($author) {
            $asset->author = $author;
            if(MarketplaceHelper::checkVerified($author)) $asset->verified = true;
            $asset->save();
            AssetHelper::clearAssetCache($asset->code, $asset->issuer);
        }
    }
}
