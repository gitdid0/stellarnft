<?php

namespace App\Jobs\Asset;

use App\Models\Asset;
use App\Helpers\AssetHelper;
use Illuminate\Bus\Queueable;
use App\Helpers\MarketplaceHelper;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class UpdateVerification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $address;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($address)
    {
        $this->address = $address;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $address = $this->address;
        $verified = MarketplaceHelper::checkVerified($address, false);
        $assets = Asset::where('author', $address)->get();
        foreach($assets as $asset) {
            $asset->verified = $verified;
            $asset->save();
            AssetHelper::clearAssetCache($asset->code, $asset->issuer);
        }
    }
}
