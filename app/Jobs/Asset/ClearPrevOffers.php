<?php

namespace App\Jobs\Asset;

use Exception;
use App\Models\Asset;
use App\Models\Offer;
use App\Helpers\OfferHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ClearPrevOffers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $asset;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Asset $asset)
    {
        $this->asset = $asset;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $asset = $this->asset;

        if(!$asset->active) return true;

        if(!$asset->owner) return true;

        $escrowSequense = OfferHelper::getAccountSequence($asset->escrow_public_key);
        if($escrowSequense) {
            Offer::where('asset_id', $asset->id)->where('sequence', '<', $escrowSequense)->delete();
            OfferHelper::clearOfferCache($asset->id, $asset->owner);
        } else {
            throw new Exception('No Escrow Sequence');
        }

        UpdateSearchable::dispatch($asset);
    }
}
