<?php

namespace App\Jobs\Activities;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NewCreatedActivity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $asset;

    public $activityData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($asset, $activityData)
    {
        $this->asset = $asset;
        $this->activityData = $activityData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $asset = $this->asset;
        $activityData = $this->activityData;

        \App\Activities\Created::addActivity($asset->id, $activityData);
    }
}
