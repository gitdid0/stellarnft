<?php

namespace App\View\Components\Profile;

use App\Models\File;
use App\Helpers\AccountHelper;
use Illuminate\View\Component;
use Illuminate\Support\Facades\Cache;

class Header extends Component
{

    public $address;

    public $profileData;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($address)
    {
        $this->address = $address;

        $profile = AccountHelper::getProfileSettings($address);

        $profileData = [
            'name' => !empty($profile['name']) ? $profile['name'] : '',
            'bio' => !empty($profile['bio']) ? $profile['bio'] : '',
            'twitter' => !empty($profile['twitter']) ? $profile['twitter'] : '',
            'instagram' => !empty($profile['instagram']) ? $profile['instagram'] : '',
            'facebook' => !empty($profile['facebook']) ? $profile['facebook'] : '',
            'reddit' => !empty($profile['reddit']) ? $profile['reddit'] : '',
            'website' => !empty($profile['website']) ? $profile['website'] : '',
            'email' => !empty($profile['email']) ? $profile['email'] : '',
            'profile_image' => !empty($profile['profile_image']) ? $profile['profile_image'] : '',
            'profile_banner' => !empty($profile['profile_banner']) ? $profile['profile_banner'] : '',
        ];

        if(!empty($profileData['profile_image'])) {
            $profileData['profile_image'] = Cache::remember('u-profile-image'.$profileData['profile_image'], 86400, function () use ($profileData) {
                return File::find($profileData['profile_image']);
            });
        }
        if(!empty($profileData['profile_banner'])) {
            $profileData['profile_banner'] = Cache::remember('u-profile-banner'.$profileData['profile_banner'], 86400, function () use ($profileData) {
                return File::find($profileData['profile_banner']);
            });
        }

        $this->profileData = $profileData;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('account.header');
    }
}
