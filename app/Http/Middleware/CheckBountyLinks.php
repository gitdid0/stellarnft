<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Helpers\BountyHelper;
use Illuminate\Support\Facades\Cookie;

class CheckBountyLinks
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $bountyRequest = $request->input(BountyHelper::$getVar);

        if(!empty($bountyRequest)) {
            BountyHelper::setBounty($bountyRequest);
            return redirect($request->path());
        }

        $address = BountyHelper::getBountyAddress();

        return $next($request);
    }
}
