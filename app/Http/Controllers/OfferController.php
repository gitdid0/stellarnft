<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\Offer;
use App\Helpers\AssetHelper;
use App\Helpers\OfferHelper;
use Illuminate\Http\Request;
use App\Helpers\AccountHelper;
use App\Jobs\Asset\UpdateSearchable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use App\Jobs\Activities\NewSaleActivity;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;

class OfferController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function offerXDR(Request $request, $code, $issuer)
    {
        $asset = Cache::rememberForever(AssetHelper::getCacheKey($code, $issuer), function() use ($code, $issuer) {
            $asset = Asset::where('code', $code)->where('issuer', $issuer)->firstOrFail();
            return $asset;
        });

        if(!$asset->active) return ['success' => false];

        if($asset->owner == session('publicKey')) return ['success' => false];

        $minOffer = $asset->version->min_offer / config('site.priceRatio');

        $data = $request->all();

        $validator_array = [
            'price' => [
                'required', 
                'numeric',
                'min:'.$minOffer,
                'max:50000000000',
                'regex:/^\d+(\.\d{1,2})?$/',
                function ($attribute, $value, $fail) use ($data) {
                    if ((floatval($value)) > floatval($data['balance'])) {
                        $fail('Insufficient balance to make the offer');
                    }
                }
            ]
        ];

        $validator = Validator::make($request->all(), $validator_array, [
            'price.required' => 'The price is required.',
            'price.numeric' => 'The price must be a number.',
            'price.min' => 'The price must be at least :min.',
            'price.max' => 'The price may not be greater than :max.',
            'price.regex' => 'The price has incorrect format.'
        ]);

        $validator->validate();

        $accountBalance = Cache::remember('balance-'.session('publicKey'), 30, function() {
            return OfferHelper::getAccountBalance(session('publicKey'));
        });

        if(!$accountBalance || (floatval($request->price)) > floatval($accountBalance)) return response()->json([
            'errors' => [
                'price' => [__('Insufficient balance to make the offer')]
            ]
        ], 422);

        $output = OfferHelper::getOfferXDR(session('publicKey'), $request->price, $asset);

        if(!$output) return ['success' => false];

        $data = [
            'owner' => $asset->owner,
            'hash' => $output['hash'],
            'price' => $request->price,
            'sequence' => $output['sequence']
        ];

        return ['success' => true, 'xdr' => $output['xdr'], 'data' => Crypt::encrypt(serialize($data))];
    }

    public function makeOffer(Request $request, $code, $issuer)
    {
        $asset = Cache::rememberForever(AssetHelper::getCacheKey($code, $issuer), function() use ($code, $issuer) {
            $asset = Asset::where('code', $code)->where('issuer', $issuer)->firstOrFail();
            return $asset;
        });

        if(!$asset->active) return ['success' => false];

        if($asset->owner == session('publicKey')) return ['success' => false];

        $xdr = $request->xdr;
        $data = $request->data;

        try {
            $data = Crypt::decrypt($data);
        } catch (\Throwable $e) {
            return ['success' => false];
        }

        $data = unserialize($data);

        $owner = $data['owner'];
        $price = $data['price'];
        $sequence = $data['sequence'];
        $hash = $data['hash'];

        $status = OfferHelper::checkOfferXDR([session('publicKey')], $xdr, $hash);

        if(!$status) return ['success' => false];

        $offer = Offer::where('asset_id', $asset->id)->where('from', session('publicKey'))->where('to', $owner)->where('sequence', $sequence)->first();

        if(!empty($offer)) {
            $offer->delete();
            OfferHelper::clearOfferCache($offer->asset_id, $offer->to);
        }

        $offer = new Offer;
        $offer->asset_id = $asset->id;
        $offer->from = session('publicKey');
        $offer->to = $owner;
        $offer->price = round($price * config('site.priceRatio'));
        $offer->sequence = $sequence;
        $offer->xdr = $xdr;
        $offer->save();

        OfferHelper::clearOfferCache($asset->id, $owner);

        UpdateSearchable::dispatch($asset);

        $notifications = AccountHelper::getNotificationsSettings($owner);

        if($notifications) {
            if(!empty($notifications['offer_received'])) {
                $delay = now()->addMinutes(5);
                Notification::route('mail', $notifications['email'])
                    ->notify((new \App\Notifications\NewOffer($asset, $offer))->delay($delay));       
            }  
        }

        $request->session()->flash('success', 'Your offer has been submitted!');

        return ['success' => true];
    }

    public function cancel(Request $request, $id)
    {
        $offer = Cache::remember('offer-'.$id, 60, function() use ($id) {
            return Offer::findOrFail($id);
        });

        if($offer->from != session('publicKey')) abort('422');

        $asset = $offer->offer_asset;

        UpdateSearchable::dispatch($asset);

        $offer->delete();

        $request->session()->flash('success', 'You have successfully canceled the offer!');

        OfferHelper::clearOfferCache($offer->asset_id, $offer->to);

        return ['success' => true];
    }

    public function accept(Request $request, $id)
    {
        $offer = Cache::remember('offer-'.$id, 60, function() use ($id) {
            return Offer::findOrFail($id);
        });

        if($offer->to != session('publicKey')) abort('422');

        $xdr = $request->xdr;

        if(empty($xdr)) return ['success' => false];

        $asset = $offer->offer_asset;

        $price = $offer->price / config('site.priceRatio');

        $sellerBalance = Cache::remember('balance-seller-'.$offer->to, 10, function() use ($offer, $asset) {
            return OfferHelper::getAccountBalance($offer->to, $asset->code.':'.$asset->issuer);
        });

        if(!$sellerBalance || (floatval($sellerBalance)) == 0) return response()->json([
            'error' => __('Please, cancel sell order for the NFT on SDEX (Stellar Decentralized Exchange) in order accept the offer.')
        ], 422);

        $buyerBalance = Cache::remember('balance-'.$offer->from, 30, function() use ($offer) {
            return OfferHelper::getAccountBalance($offer->from);
        });

        if(!$buyerBalance || (floatval($price)) > floatval($buyerBalance)) return response()->json([
            'error' => __('The buyer does not have sufficient balance to fulfill the offer, please, try again later.')
        ], 422);

        $tx = OfferHelper::acceptOffer($xdr, $offer, $asset);

        if(!$tx || empty($tx['successful']) || !$tx['successful']) return ['success' => false];

        NewSaleActivity::dispatch($asset, [
            'from' => $offer->to,
            'to' => $offer->from,
            'price' => $offer->price,
            'tx' => $tx['id']
        ]);

        $notifications = AccountHelper::getNotificationsSettings($offer->from);

        if($notifications) {
            if(!empty($notifications['offer_accepted'])) {
                Notification::route('mail', $notifications['email'])
                    ->notify(new \App\Notifications\OfferAccepted($asset));       
            }  
        }

        $royalty = OfferHelper::getOfferRoyalty($offer->price, $asset);
        if($royalty != false) {
            $data = $asset->dataentries;
            $notifications = AccountHelper::getNotificationsSettings($data['royaltyReceiver']);

            if($notifications) {
                if(!empty($notifications['new_royalty'])) {
                    Notification::route('mail', $notifications['email'])
                        ->notify(new \App\Notifications\NewRoyalty());       
                }  
            }
        }

        $request->session()->flash('success', 'You have successfully accepted the offer!');

        return ['success' => true, 'url' => $asset->url];
    }
}
