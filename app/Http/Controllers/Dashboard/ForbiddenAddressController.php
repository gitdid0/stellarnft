<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Models\ForbiddenAddress;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class ForbiddenAddressController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addresses = ForbiddenAddress::latest()->paginate(10);

        return view('dashboard.forbidden-addresses.index', compact('addresses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.forbidden-addresses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address' => ['required', 'unique:forbidden_addresses,address']
        ])->validate();

        $forbiddenAddress = new ForbiddenAddress;
        $forbiddenAddress->address = $request->address;
        $forbiddenAddress->save();

        return redirect()->route('dashboard.forbidden-addresses.index')->withSuccess('The address has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $forbiddenAddress = ForbiddenAddress::findOrFail($id);

        $forbiddenAddress->delete();

        return redirect()->back()->withSuccess('The address has been deleted!');
    }
}
