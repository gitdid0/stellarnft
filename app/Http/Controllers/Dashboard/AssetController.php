<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Asset;
use App\Helpers\AssetHelper;
use Illuminate\Http\Request;
use App\Jobs\Asset\ForceDelete;
use App\Http\Controllers\Controller;
use App\Jobs\Asset\UpdateSearchable;

class AssetController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets = Asset::latest()->paginate(10);

        return view('dashboard.assets.index', compact('assets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset = Asset::findOrFail($id);

        return view('dashboard.assets.edit', compact('asset'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $asset = Asset::findOrFail($id);

        $banned = $request->banned ? true : false;

        $nsfw = $request->nsfw ? true : false;

        if($asset->banned != $banned) {
            $asset->banned = $banned;

            $asset->save();

            AssetHelper::clearAssetCache($asset->code, $asset->issuer);    
        }

        if($asset->version->nsfw != $nsfw) {
            $asset->version->nsfw = $nsfw;

            $asset->version->save();

            AssetHelper::clearAssetCache($asset->code, $asset->issuer); 
            UpdateSearchable::dispatch($asset);  
        }

        return redirect()->back()->withSuccess('Asset successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $asset = Asset::findOrFail($id);

        ForceDelete::dispatch($asset);

        return redirect()->back()->withSuccess('The item queued for deletion!');
    }
}
