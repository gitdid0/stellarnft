<?php

namespace App\Http\Controllers;

use App\Helpers\StellarHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NftController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('nfts.create');
    }

    public function validateData(Request $request)
    {
        $data = $request->all();

        $validator_array = [
            'code' => ['required', 'alpha_num', 'max:12'],
            'name' => [
                'nullable', 
                function ($attribute, $value, $fail) use ($request) {
                    if (strlen($value) > 64) {
                        $fail(__('The name can only contain a maximum of 64 bytes (64 characters).'));
                    }
                }
            ],
            'ipfs_cid' => [
                'nullable', 
                function ($attribute, $value, $fail) use ($request) {
                    if (strlen($value) > 64) {
                        $fail(__('IPFS CID can only contain a maximum of 64 bytes (64 characters).'));
                    }
                }
            ],
            'royalty' => [
                'nullable',
                'numeric',
                'min:0.01',
                'max:'.(config('site.maxRoyaltyReward') * 100),
                'regex:/^\d+(\.\d{1,2})?$/'
            ],
            'home_domain' => [
                'nullable',
                function ($attribute, $value, $fail) use ($request) {
                    $value = trim($value, '. \n\r\t\v\0');
                    if (!filter_var($value, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME) || (strpos($value, '.') === false)) {
                        $fail(__('Must be a valid domain without an http protocol e.g. google.com, www.google.com'));
                    }
                }
            ],
            'dataentries.*.name' => [
                'nullable',
                'regex:/^[a-zA-Z0-9 !@#$%^&*()_+.,<>?|\/\\\\]+$/',
                'required_with:dataentries.*.value', 
                'not_in:name,ipfs,royaltyAmount,royaltyReceiver',
                function ($attribute, $value, $fail) use ($request) {
                    if (strlen($value) > 64) {
                        $fail(__('The name can only contain a maximum of 64 bytes (64 characters).'));
                    }
                }
            ],
            'dataentries.*.value' => [
                'nullable', 
                'required_with:dataentries.*.name', 
                function ($attribute, $value, $fail) use ($request) {
                    if (strlen($value) > 64) {
                        $fail(__('The value can only contain a maximum of 64 bytes (64 characters).'));
                    }
                }
            ]
        ];

        $validator = Validator::make($request->all(), $validator_array, [
            'code.required' => 'The NFT Code is required.',
            'code.alpha_num' => 'The NFT Code may only contain letters and numbers',
            'code.max' => 'The NFT Code may not be greater than :max characters.',
            
            'royalty.numeric' => 'The royalty must be a number.',
            'royalty.min' => 'The royalty must be at least :min.',
            'royalty.max' => 'The royalty may not be greater than :max.',
            'royalty.regex' => 'The royalty must be between 0.01 and '.(config('site.maxRoyaltyReward') * 100),

            'dataentries.*.value.required_with' => 'The value is required if the name is present',
            'dataentries.*.name.required_with' => 'The name is required if the value is present',
            'dataentries.*.name.not_in' => 'The name may not be "name", "ipfs", "royaltyAmount", "royaltyReceiver"',
            'dataentries.*.name.regex' => 'Invalid name',
        ]);

        $validator->validate();

        return true;
    }

    public function show($code, $issuer)
    {
        $validator = Validator::make([
            'code' => $code,
            'issuer' => $issuer
        ], [
            'code' => ['required', 'alpha_num', 'max:12'],
            'issuer' => ['required', function ($attribute, $value, $fail) {
                if (!StellarHelper::isValidAccountId($value)) {
                    $fail('Enter correct Stellar Address');
                }
            }]
        ]);

        if ($validator->fails()) {
            return redirect()->route('nfts.create');
        }

        return view('nfts.show', compact('code', 'issuer'));
    }
}
