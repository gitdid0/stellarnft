<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\Request;
use App\Helpers\MarketplaceHelper;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getServerIdFromPath($path)
    {
        return Crypt::encryptString($path);
    }

    public function upload($name, Request $request)
    {
        $data = $request->all();

        $validatorData = false;
        $validatorErrors = [];
        if($name == 'image') {
            $validatorData = 'required|mimes:jpeg,png,jpg,gif|max:10240';
            $validatorErrors = [
                $name.'.mimes' => 'Not supported format',
                $name.'.max' => 'The file may not be greater than 10 MB.'
            ];
        }
        if($name == 'audio') {
            $validatorData = 'required|mimes:mp3|max:10240';
            $validatorErrors = [
                $name.'.mimes' => 'Not supported format',
                $name.'.max' => 'The file may not be greater than 10 MB.'
            ];
        }
        if($name == 'video') {
            $validatorData = 'required|mimes:mp4|max:30720';
            $validatorErrors = [
                $name.'.mimes' => 'Not supported format',
                $name.'.max' => 'The file may not be greater than 30 MB.'
            ];
        }

        $verified = MarketplaceHelper::checkVerified(session('publicKey'));

        if($name == 'profile_image' && $verified) {
            $validatorData = 'required|mimes:jpeg,png,jpg|max:2048';
            $validatorErrors = [
                $name.'.mimes' => 'Not supported format',
                $name.'.max' => 'The file may not be greater than 2 MB.'
            ];
        }
        if($name == 'profile_banner' && $verified) {
            $validatorData = 'required|mimes:jpeg,png,jpg|max:2048';
            $validatorErrors = [
                $name.'.mimes' => 'Not supported format',
                $name.'.max' => 'The file may not be greater than 2 MB.'
            ];
        }

        if(!$validatorData) return Response::make('Error', 422, [
            'Content-Type' => 'text/plain',
        ]);

        $validator = Validator::make($request->all(), [
            $name => $validatorData
        ], $validatorErrors);

        $input = $request->file($name);

        if ($input === null) {
            return Response::make('File is required', 422, [
                'Content-Type' => 'text/plain',
            ]);
        }

        if ($validator->fails()) {
            return Response::make($validator->errors()->first(), 422, [
                'Content-Type' => 'text/plain',
            ]);
        }

        $file = is_array($input) ? $input[0] : $input;
        $path = 'tmp';
        $disk = 'local';

        if (! ($newFile = $file->store($path, $disk))) {
            return Response::make('Could not save file', 500, [
                'Content-Type' => 'text/plain',
            ]);
        }

        return Response::make($this->getServerIdFromPath($newFile), 200, [
            'Content-Type' => 'text/plain',
        ]);
    }

    public function restore($name, Request $request)
    {
        $file = File::findOrFail(Crypt::decryptString($request->restore));

        if($file->path) {
            $pathToFile = storage_path('app/'.$file->path);
            abort_if(!file_exists($pathToFile), 404);
            return response()->file($pathToFile);
        } elseif ($file->object_path) {
            return Storage::disk('s3')->response($file->object_path);
        } 

        abort(404);
    }

    public function delete($name, Request $request)
    {
        return Response::make('', 200, [
            'Content-Type' => 'text/plain',
        ]);
    }

    public function show($id, $file, Request $request)
    {
        return redirect(config('site.static_files_host').'asset-version-'.$id.'/'.$file);
    }
}
