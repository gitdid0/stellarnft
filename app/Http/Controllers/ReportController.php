<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Helpers\AssetHelper;
use Illuminate\Http\Request;
use App\Notifications\NewReport;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;

class ReportController extends Controller
{
    public function report(Request $request, $code, $issuer)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(),[
            'details' => ['required']
        ], [
            'details.required' => 'The message is required'
        ]);

        $validator->validate();

        $asset = AssetHelper::getAssetByCode($code, $issuer);

        if(!$asset) {
            return response()->json([
                'errors' => [
                    'details' => [__('An error occurred, please try again later.')]
                ]
            ], 422);
        }

        $reportCacheKey = 'report-' . session()->getId() . '-' . $asset->id;

        if(Cache::has($reportCacheKey)) {
            return response()->json([
                'errors' => [
                    'details' => [__('You already reported this item.')]
                ]
            ], 422);
        }

        Cache::put($reportCacheKey, true, 3600);

        $report = new Report;
        $report->asset_id = $asset->id;
        $report->details = $request->details;
        $report->save();

        if(config('site.slack_hook'))
            Notification::route('slack', config('site.slack_hook'))
                ->notify(new NewReport([
                    'url' => $asset->url,
                    'details' => $report->details
                ]));

        return ['success' => true];
    }
}
