<?php

namespace App\Http\Controllers;

use App\Helpers\FileHelper;
use App\Models\Setting;
use App\Jobs\RemoveFile;
use Illuminate\Http\Request;
use App\Helpers\AccountHelper;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return redirect()->route('settings.general');
    }

    public function profileSettings(Request $request)
    {
        $profile = AccountHelper::getProfileSettings(session('publicKey'));

        $profileData = [
            'name' => !empty($profile['name']) ? $profile['name'] : '',
            'bio' => !empty($profile['bio']) ? $profile['bio'] : '',
            'twitter' => !empty($profile['twitter']) ? $profile['twitter'] : '',
            'instagram' => !empty($profile['instagram']) ? $profile['instagram'] : '',
            'facebook' => !empty($profile['facebook']) ? $profile['facebook'] : '',
            'reddit' => !empty($profile['reddit']) ? $profile['reddit'] : '',
            'website' => !empty($profile['website']) ? $profile['website'] : '',
            'email' => !empty($profile['email']) ? $profile['email'] : '',
            'profile_image' => !empty($profile['profile_image']) ? Crypt::encryptString($profile['profile_image']) : '',
            'profile_banner' => !empty($profile['profile_banner']) ? Crypt::encryptString($profile['profile_banner']) : ''
        ];

        return view('settings.profile', compact('profile', 'profileData'));
    }

    public function setProfileSettings(Request $request)
    {
        $settings = AccountHelper::getSettings(session('publicKey'));

        $validator_array = [
            'name' => ['nullable', 'max:42', 'string'],
            'bio' => ['nullable', 'max:300', 'string'],
            'twitter' => ['nullable', 'url'],
            'instagram' => ['nullable', 'url'],
            'facebook' => ['nullable', 'url'],
            'reddit' => ['nullable', 'url'],
            'website' => ['nullable', 'url'],
            'email' => ['nullable', 'email'],
        ];

        $validator = Validator::make($request->all(), $validator_array, [
            'name.max' => 'The name may not be greater than :max characters.',
            'description.max' => 'The bio may not be greater than :max characters.',

            'twitter.url' => 'Invalid url.',
            'instagram.url' => 'Invalid url.',
            'facebook.url' => 'Invalid url.',
            'reddit.url' => 'Invalid url.',
            'website.url' => 'Invalid url.',
            'email.email' => 'The email must be a valid email address.'
        ]);

        $validator->validate();

        $data = [];

        if(empty($settings)) {
            $settings = new Setting;
            $settings->address = session('publicKey');
        } else {
            $data = $settings->data;
        }

        $profile = [];

        $profile['name'] = $request->name;
        $profile['bio'] = $request->bio;
        $profile['twitter'] = $request->twitter;
        $profile['facebook'] = $request->facebook;
        $profile['instagram'] = $request->instagram;
        $profile['reddit'] = $request->reddit;
        $profile['website'] = $request->website;
        $profile['email'] = $request->email;
        $profile['profile_image'] = false;
        $profile['profile_banner'] = false;

        if(!empty($request->profile_image)) {
            $profile['profile_image'] = FileHelper::getUpdatedFileId($request->profile_image, 'u-'.session('publicKey').'-'.now()->timestamp);
        }

        if(!empty($request->profile_banner)) {
            $profile['profile_banner'] = FileHelper::getUpdatedFileId($request->profile_banner, 'u-'.session('publicKey').'-'.now()->timestamp);
        }

        if(!empty($data['profile']) && !empty($data['profile']['profile_image'])) RemoveFile::dispatch($data['profile']['profile_image']);

        if(!empty($data['profile']) && !empty($data['profile']['profile_banner'])) RemoveFile::dispatch($data['profile']['profile_banner']);

        $data['profile'] = $profile;

        $settings->data = $data;

        $settings->save();

        AccountHelper::clearSettingsCache(session('publicKey'));

        $request->session()->flash('success', 'Profile settings have been updated!');

        return ['success' => true, 'url' => route('settings.profile')];
    }

    public function generalSettings(Request $request)
    {
        $general = AccountHelper::getGeneralSettings(session('publicKey'));

        return view('settings.general', compact('general'));
    }

    public function setGeneralSettings(Request $request)
    {
        $settings = AccountHelper::getSettings(session('publicKey'));

        $data = [];

        if(empty($settings)) {
            $settings = new Setting;
            $settings->address = session('publicKey');
        } else {
            $data = $settings->data;
        }

        $general = [];

        if($request->bounty_program) {
            $general['bounty_program'] = true;
        }

        $data['general'] = $general;

        $settings->data = $data;

        $settings->save();

        AccountHelper::clearSettingsCache(session('publicKey'));

        return back()->withSuccess('General settings have been updated!');
    }

    public function notifications(Request $request)
    {
        $notifications = AccountHelper::getNotificationsSettings(session('publicKey'));

        return view('settings.notifications', compact('notifications'));
    }

    public function setNotifications(Request $request)
    {
        $validator_array = [
            'email' => ['nullable', 'email'],
        ];

        $validator = Validator::make($request->all(), $validator_array, [
            'email.email' => 'The email must be a valid email address.',
            'offers_received.in' => 'The selected attribute is invalid.'
        ]);

        $validator->validate();

        $settings = AccountHelper::getSettings(session('publicKey'));

        $data = [];

        if(empty($settings)) {
            $settings = new Setting;
            $settings->address = session('publicKey');
        } else {
            $data = $settings->data;
        }

        $notifications = [];

        if(!empty($request->email)) {
            $notifications['email'] = $request->email;
            if(!empty($request->offer_received)) {
                $notifications['offer_received'] = true;
            }
            if(!empty($request->offer_accepted)) {
                $notifications['offer_accepted'] = true;
            }
            if(!empty($request->new_royalty)) {
                $notifications['new_royalty'] = true;
            }
        }

        $data['notifications'] = $notifications;

        $settings->data = $data;

        $settings->save();

        AccountHelper::clearSettingsCache(session('publicKey'));

        return back()->withSuccess('Notification preferences have been updated!');
    }
}
