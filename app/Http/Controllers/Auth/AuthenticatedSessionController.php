<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\PythonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthenticatedSessionController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('destroy');
        $this->middleware('auth')->only('destroy');
    }

    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $sessionCode = hash('sha256', session()->getId());

        return view('auth.login', compact('sessionCode'));
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $xdr = $request->xdr;
        $publicKey = $request->publicKey;
        $sessionCode = hash('sha256', session()->getId());
        $wallet = $request->wallet;

        $supportedWallets = config('site.wallets');

        $status = PythonHelper::checkAuthTransaction($xdr, $sessionCode, $publicKey);

        if($status && !empty($supportedWallets[$wallet])) {
            $request->session()->regenerate();

            session([
                'wallet' => $wallet,
                'publicKey' => $publicKey,
                'auth' => true
            ]);

            $request->session()->flash('success', 'You have successfully logged in!');

            if($request->backurl && strpos($request->backurl, config('app.url')) !== false) {
                return ['status' => true, 'url' => $request->backurl];
            }

            return ['status' => true, 'url' => route('home')];
        }

        return ['status' => false];
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        $request->session()->invalidate();

        return redirect()->route('home');
    }
}
