<?php

namespace App\Helpers;

use App\Models\Category;
use App\Models\VerifiedAddress;
use Illuminate\Support\Facades\Cache;

class MarketplaceHelper
{
    public static function getCacheKey($currency = 'usd')
    {
        return 'XLM-PRICE-'.strtoupper($currency);
    }

    public static function getXLMPrice($amount = '10000000', $formated = false, $currency = 'usd')
    {
        $pricePerXLM = Cache::get(self::getCacheKey($currency), '0');

        $price = $pricePerXLM * ($amount / config('site.priceRatio'));

        if($formated) return trimTrailingZeroes(number_format($price, 2, '.', ','));

        return $price;
    }

    public static function setXLMPrice($value, $currency = 'usd')
    {
        Cache::put(self::getCacheKey($currency), $value);

        return true;
    }

    public static function checkVerified($address, $cache = true)
    {
        if($cache && Cache::has('verified-'.$address)) return Cache::get('verified-'.$address, false);

        $verified = false;

        $verifiedData = VerifiedAddress::where('address', $address)->first();

        if(!empty($verifiedData)) $verified = true;
        
        Cache::put('verified-'.$address, $verified);
        
        return $verified;
    }

    public static function getCategories($cache = true)
    {
        if($cache && Cache::has('categories')) return Cache::get('categories', []);

        $categoryArray = [];

        $categories = Category::whereNull('parent_id')->orderBy('sort')->get();

        foreach($categories as $category) {
            $children = Category::where('parent_id', $category->id)->orderBy('sort')->get();
            $childrenArray = [];
            $categoryArray[] = [
                'id' => $category->id,
                'name' => $category->name,
                'icon' => $category->icon,
                'children' => $childrenArray
            ];
        }
        
        Cache::put('categories', $categoryArray);
        
        return $categoryArray;
    }

    public static function getCategoriesArray($categories = false)
    {
        if($categories == false) $categories = Cache::get('categories', []);

        $ids = [];

        $categories = Cache::get('categories', []);

        foreach($categories as $category) {
            $ids[] = $category['id'];
            if(!empty($category['children'])) {
                $children = self::getCategoriesArray($category['children']);
                foreach($children as $child) $ids[] = $child['id'];
            }
        }
        
        return $ids;
    }
}