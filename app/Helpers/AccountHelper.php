<?php

namespace App\Helpers;

use App\Models\Setting;
use Illuminate\Support\Facades\Cache;

class AccountHelper
{
    public static function getNotificationsSettings($address = '') 
    {
        if(empty($address)) {
            return [];
        }

        $settings = self::getSettings($address);

        if(!empty($settings)) {
            $data = $settings->data;
            if(!empty($data['notifications'])) {
                return $data['notifications'];
            }
        }

        return [];
    }

    public static function getGeneralSettings($address = '') 
    {
        if(empty($address)) {
            return [];
        }

        $settings = self::getSettings($address);

        if(!empty($settings)) {
            $data = $settings->data;
            if(!empty($data['general'])) {
                return $data['general'];
            }
        }

        return [];
    }

    public static function getProfileSettings($address = '') 
    {
        if(empty($address)) {
            return [];
        }

        $settings = self::getSettings($address);

        if(!empty($settings)) {
            $data = $settings->data;
            if(!empty($data['profile'])) {
                return $data['profile'];
            }
        }

        return [];
    }

    public static function getSettings($address = '') 
    {
        if(empty($address)) {
            return false;
        }

        $settings = Cache::get(self::getSettingsCacheKey($address), function() use ($address) {
            $settings = Setting::where('address', $address)->first();

            if(!empty($settings)) return $settings;

            return false;
        });

        return $settings;
    }

    public static function clearSettingsCache($address = '') 
    {
        if(empty($address)) {
            return false;
        }

        Cache::forget(self::getSettingsCacheKey($address));

        return true;
    }

    public static function getSettingsCacheKey($address = '')
    {
        if(empty($address)) {
            return '';
        }

        return 'settings-'.strtoupper($address);
    }
}