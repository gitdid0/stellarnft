<?php

namespace App\Helpers;

use App\Helpers\StellarHelper;
use Illuminate\Support\Facades\Cookie;

class BountyHelper
{
    public static $cookieVar = 'bountyAddress';

    public static $getVar = 'b';

    public static $cookieTimeout = 43200;

    public static function getRferralLink($asset)
    {
        if(empty(session('publicKey'))) return '';

        return $asset->url . '?'.self::$getVar.'='.session('publicKey');
    }

    public static function showBountyInfo() 
    {
        if(session('auth')) {
            $generalSettings = AccountHelper::getGeneralSettings(session('publicKey'));
            if(!empty($generalSettings['bounty_program'])) return true;
        }

        return false;
    }

    public static function setBounty($bountyAddress) 
    {
        $cookieParam = Cookie::get(self::$cookieVar);

        if(empty($cookieParam)) {
            if (StellarHelper::isValidAccountId($bountyAddress)) {
                Cookie::queue(self::$cookieVar, $bountyAddress, self::$cookieTimeout);
            }
        }
    }

    public static function getBountyAddress() 
    {
        $bountyAddress = Cookie::get(self::$cookieVar);

        if(!empty($bountyAddress)) {
            return $bountyAddress;
        }

        return false;
    }

    public static function getBountyValue($bounty, $price, $format = false)
    {
        $price = $price * ($bounty * config('site.stroop') * 0.01);

        if($format) return formatPrice($price);

        return $price;
    }
}