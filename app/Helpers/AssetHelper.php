<?php

namespace App\Helpers;

use App\Helpers\FileHelper;
use App\Models\Asset;
use App\Jobs\Asset\CheckActive;
use Illuminate\Support\Facades\Cache;

class AssetHelper
{
    public static function checkActive($asset) 
    {
        if($asset->active) return true;

        $dispatchJob = Cache::rememberForever('check-active-'.$asset->id, function () use ($asset) {
            CheckActive::dispatch($asset);
            return true;
        });

        return true;
    }

    public static function getAssetByCode($code = '', $issuer = '') 
    {
        if(empty($code) || empty($issuer)) {
            return false;
        }

        $asset = Cache::get(self::getCacheKey($code, $issuer), function() use ($code, $issuer) {
            $asset = Asset::where('code', $code)->where('issuer', $issuer)->first();
            if($asset) return $asset;
            return false;
        });

        if(!empty($asset)) Cache::put(AssetHelper::getCacheKey($code, $issuer), $asset);

        return $asset;
    }

    public static function clearAssetCache($code = '', $issuer = '') 
    {
        if(empty($code) || empty($issuer)) {
            return false;
        }

        Cache::forget(self::getCacheKey($code, $issuer));

        return true;
    }

    public static function getCacheKey($code = '', $issuer = '')
    {
        if(empty($code) || empty($issuer)) {
            return '';
        }

        return 'asset-'.strtoupper($code).'-'.strtoupper($issuer);
    }

    public static function deleteAsset($xdr, $asset)
    {
        $tx = PythonHelper::deleteAsset(
            $asset->owner,
            $xdr,
            $asset->escrow_secret_key
        );

        return $tx;
    }

    public static function forceDeleteAsset($asset)
    {
        $destination = !empty($asset->owner) ? $asset->owner : config('site.site_wallet_address');

        $tx = PythonHelper::forceDeleteAsset(
            $destination,
            $asset->escrow_secret_key
        );

        return $tx;
    }

    public static function deleteAssetVersion($assetVersion)
    {
        FileHelper::removeFile($assetVersion->image_id);

        FileHelper::removeFile($assetVersion->audio_id);

        FileHelper::removeFile($assetVersion->video_id);

        $assetVersion->delete();
    }

    public static function fromSearchablesToList($assets)
    {
        foreach($assets as $key => $asset) {

            $asset = AssetHelper::getAssetByCode($asset->code, $asset->issuer);

            if(empty($asset)) continue;

            $assets[$key]['url'] = $asset->url;

            $assets[$key]['image'] = $asset->version->thumb(260);

            $assets[$key]['audio'] = $asset->version->audio ? $asset->version->audio->url : '';

            $assets[$key]['video'] = $asset->version->video ? $asset->version->video->url : '';

            $assets[$key]['price'] = formatPrice($assets[$key]['price']);

            $assets[$key]['lockable'] = !empty($asset->version->lockable);

        }

        return $assets;
    }

    public static function getSearchData($asset)
    {
        $version = $asset->version;

        $lastSale = $asset->activities()->where('type', '\App\Activities\Sale')->orderByDesc('id')->first();

        $searchData = [
            'asset_version_id' => $version->id,
            'code' => $asset->code,
            'issuer' => $asset->issuer,
            'author' => $asset->author,
            'owner' => $asset->owner,
            'active' => $asset->active,
            'banned' => $asset->banned,
            'nsfw' => $version->nsfw,
            'private' => $version->private,
            'verified' => $asset->verified,
            'price' => $version->price,
            'bounty' => $version->bounty,
            'name' => $version->name,
            'has_offers' => $asset->offers()->count() > 0 ? true : false,
            'sold_at' => !empty($lastSale) ? $lastSale->created_at : null,
            'created_at' => $asset->created_at,
        ];

        return $searchData;
    }
}