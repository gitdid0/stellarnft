<?php

namespace App\Helpers\Stellar;

interface CRCInterface
{
    public function reset();

    public function update($data);

    public function finish();
}