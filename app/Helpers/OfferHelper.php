<?php

namespace App\Helpers;

use App\Helpers\StellarHelper;
use App\Helpers\BountyHelper;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;

class OfferHelper
{
    public static function clearOfferCache($assetId, $owner) 
    {
        Cache::forget(self::getCacheKey($assetId, $owner));

        return true;
    }

    public static function getCacheKey($assetId, $owner, $trade = false)
    {
        return 'offers-'.$assetId.'-'.$owner.'-'.($trade ? 'trade' : 'buy');
    }

    public static function getAccountSequence($public_key)
    {
        $horizonServer = config('site.horizonServer');

        $response = Http::get($horizonServer. 'accounts/' . $public_key);

        if($response->ok()) {
            $account = $response->json();
            
            if(!empty($account['sequence'])) return $account['sequence'];
        }

        return false;
    }

    public static function getAccountBalance($public_key, $type = 'native')
    {
        $horizonServer = config('site.horizonServer');

        $response = Http::get($horizonServer. 'accounts/' . $public_key);

        if($response->ok()) {
            $account = $response->json();
            
            $balances = $account['balances'];

            $minBalance = (3 + $account['subentry_count'] + $account['num_sponsoring'] - $account['num_sponsored']) * config('site.baseReserve');

            foreach($balances as $balance) {
                if($type == 'native') {
                    if($balance['asset_type'] == 'native') {
                        return ($balance['balance'] - $minBalance - $balance['selling_liabilities']); 
                    } 
                } else {
                    if(empty($balance['asset_code']) || empty($balance['asset_issuer'])) continue;
                    if($type == $balance['asset_code'].':'.$balance['asset_issuer']) return ($balance['balance'] - $balance['selling_liabilities']);
                }
            }
        }

        return false;
    }

    public static function getOfferXDR($from, $amount, $asset)
    {
        $escrowSequence = self::getAccountSequence($asset->escrow_public_key);

        if(empty($escrowSequence)) return false;

        if(empty($asset->owner)) return false;

        $data = $asset->dataentries;

        $finalAmount = $amount;

        $operations = [];

        $operations[] = [
            'type' => 'change_trust',
            'source' => $from,
            'asset_code' => $asset->code,
            'asset_issuer' => $asset->issuer,
            'limit' => '1'
        ];

        $operations[] = [
            'type' => 'payment',
            'source' => $from,
            'asset_code' => 'XLM',
            'asset_issuer' => '',
            'amount' => $finalAmount,
            'destination' => $asset->owner
        ];   

        $operations[] = [
            'type' => 'payment',
            'source' => $asset->owner,
            'asset_code' => $asset->code,
            'asset_issuer' => $asset->issuer,
            'amount' => '0.0000001',
            'destination' => $from
        ];

        $operations[] = [
            'type' => 'change_trust',
            'source' => $asset->owner,
            'asset_code' => $asset->code,
            'asset_issuer' => $asset->issuer,
            'limit' => '0'
        ];

        $royalty = self::getOfferRoyalty($amount, $asset);

        if($royalty != false) {
            $operations[] = [
                'type' => 'claimable_balance',
                'source' => $asset->owner,
                'asset_code' => 'XLM',
                'asset_issuer' => '',
                'amount' => $royalty,
                'destination' => $data['royaltyReceiver'],
                'relative_time' => config('site.royaltyDeadline')
            ];    
        }

        $bounty = self::getOfferBounty($amount, $asset);

        if($bounty != false) {
            $bountyAddress = BountyHelper::getBountyAddress();
            if(
                !empty($bountyAddress)
                && $bountyAddress != $asset->owner
                && $bountyAddress != $from
            ) {
                $operations[] = [
                    'type' => 'claimable_balance',
                    'source' => $asset->owner,
                    'asset_code' => 'XLM',
                    'asset_issuer' => '',
                    'amount' => $bounty,
                    'destination' => $bountyAddress,
                    'relative_time' => config('site.bountyDeadline')
                ];   
            }
        }

        $output = PythonHelper::getXDR([
            'public' => $asset->escrow_public_key,
            'secret' => $asset->escrow_secret_key,
            'sequence' => $escrowSequence,
            'memo' => $asset->id
        ], $operations);

        $output['sequence'] = $escrowSequence;

        if(!$output) return false;

        return $output;
    }

    public static function checkOfferXDR($signers, $xdr, $hash)
    {
        $xdr = PythonHelper::checkOfferXDR(
            $signers,
            $xdr, 
            $hash
        );

        return $xdr;
    }

    public static function acceptOffer($xdr, $offer, $asset)
    {
        $tx = PythonHelper::acceptOffer(
            [$offer->from, $offer->to],
            $xdr,
            $offer->xdr,
            $asset->escrow_secret_key
        );

        return $tx;
    }

    public static function getOfferRoyalty($amount, $asset)
    {
        $data = $asset->dataentries;

        $allowRewards = $amount >= (config('site.baseReserve') * config('site.rewardsBaseReserve'));

        if(
            !empty($data['royaltyAmount']) 
            && !empty($data['royaltyReceiver']) 
            && StellarHelper::isValidAccountId($data['royaltyReceiver'])
            && $data['royaltyReceiver'] != $asset->owner
        ) {
            $royalty = intval($data['royaltyAmount']) * config('site.stroop') * 0.01;
            if($royalty > config('site.maxRoyaltyReward')) $royalty = config('site.maxRoyaltyReward');
            $royaltyAmount = round($royalty * $amount, 7);

            if($allowRewards) {
                $royaltyAmount = strval($royaltyAmount);     

                return $royaltyAmount;
            } 
        }

        return false;
    }

    public static function getOfferBounty($amount, $asset)
    {
        if($asset->version->bounty > 0) {
            $allowRewards = $amount >= (config('site.baseReserve') * config('site.rewardsBaseReserve'));

            $bounty = intval($asset->version->bounty) * config('site.stroop') * 0.01;
            if($bounty > config('site.maxBountyReward')) $bounty = config('site.maxBountyReward');
            $bountyAmount = round($bounty * $amount, 7);
            if($allowRewards) {
                $bountyAmount = strval($bountyAmount);     
                return $bountyAmount;
            }
        }

        return false;
    }
}