<?php

namespace App\Helpers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;

class PythonHelper
{
    public static $server = 'localhost:5000';

    public static function checkAuthTransaction($xdr, $sessionCode, $publicKey) 
    {
        $response = Http::post(self::$server . '/check-auth-transaction', [
            'xdr' => $xdr,
            'sessionCode' => $sessionCode,
            'publicKey' => $publicKey
        ]);

        if($response->ok()) {
            $json = $response->json();
            if($json) {
                return true;
            }
        }

        return false;
    }

    public static function checkOfferXDR($signers, $xdr, $hash) 
    {
        $response = Http::post(self::$server . '/check-offer-xdr', [
            'signers' => $signers,
            'xdr' => $xdr,
            'hash' => $hash,
            'test' => App::environment('production') ? false : true
        ]);

        if($response->ok()) {
            $json = $response->json();
            if($json) {
                return true;
            }
        }

        return false;
    }

    public static function acceptOffer($signers, $signedXDR, $xdr, $escrowSecret)
    {
        $response = Http::post(self::$server . '/accept-offer', [
            'signers' => $signers,
            'signed_xdr' => $signedXDR,
            'xdr' => $xdr,
            'horizon' => config('site.horizonServer'),
            'site_secret' => config('site.site_wallet_secret'),
            'escrow_secret' => $escrowSecret,
            'test' => App::environment('production') ? false : true
        ]);

        if($response->ok()) {
            $json = $response->json();
            if($json) {
                return $json;
            }
        }

        return false;
    }

    public static function deleteAsset($owner, $xdr, $escrowSecret)
    {
        $response = Http::post(self::$server . '/delete-asset', [
            'owner' => $owner,
            'xdr' => $xdr,
            'escrow_secret' => $escrowSecret,
            'horizon' => config('site.horizonServer'),
            'site_secret' => config('site.site_wallet_secret'),
            'escrow_secret' => $escrowSecret,
            'test' => App::environment('production') ? false : true
        ]);

        if($response->ok()) {
            $json = $response->json();
            if($json) {
                return $json;
            }
        }

        return false;
    }

    public static function forceDeleteAsset($owner, $escrowSecret)
    {
        $response = Http::post(self::$server . '/force-delete-asset', [
            'owner' => $owner,
            'escrow_secret' => $escrowSecret,
            'horizon' => config('site.horizonServer'),
            'site_secret' => config('site.site_wallet_secret'),
            'escrow_secret' => $escrowSecret,
            'test' => App::environment('production') ? false : true
        ]);

        if($response->ok()) {
            $json = $response->json();
            if($json) {
                return $json;
            }
        }

        return false;
    }

    public static function getXDR($signer, $operations) 
    {
        $response = Http::post(self::$server . '/get-xdr', [
            'signer' => $signer,
            'operations' => $operations,
            'test' => App::environment('production') ? false : true
        ]);

        if($response->ok()) {
            $json = $response->json();
            if($json) {
                return $json;
            }
        }

        return false;
    }

    public static function getKeypair() 
    {
        $response = Http::post(self::$server . '/get-keypair');

        if($response->ok()) {
            $json = $response->json();
            if($json) {
                return $json;
            }
        }

        return false;
    }
}