<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Cache;

class ActivityHelper
{
    public static function clearCache($assetId) 
    {
        Cache::forget(self::getCacheKey($assetId));

        return true;
    }

    public static function getCacheKey($assetId)
    {
        return 'activities-'.$assetId;
    }
}