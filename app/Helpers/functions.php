<?php

if (! function_exists('truncStellarAddress')) 
{
    function truncStellarAddress($address) 
    {
    	return substr($address, 0, 4).'...'.substr($address, -4);
    }
}

if (! function_exists('trimTrailingZeroes')) 
{
    function trimTrailingZeroes($nbr) {
        return strpos($nbr,'.')!==false ? rtrim(rtrim($nbr,'0'),'.') : $nbr;
    }
}

if (! function_exists('formatPrice')) 
{
    function formatPrice($nbr) {
        return trimTrailingZeroes(number_format(($nbr / config('site.priceRatio')), 2, '.', ','));
    }
}

if (! function_exists('getPrevUrl')) 
{
    function getPrevUrl() {
        $prevUrl = '';

        if(!empty(session('prevUrl'))) {
            $prevUrl = session('prevUrl');
            session()->forget('prevUrl');
        } else {
            $prevUrl = url()->previous();
        }

        return $prevUrl;
    }
}