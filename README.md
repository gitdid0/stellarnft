## About

Digital marketplace for non-fungible tokens (NFTs) on Stellar blockchain

## License

StellarNFT is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
