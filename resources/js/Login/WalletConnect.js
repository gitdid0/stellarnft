import WalletConnectClient, { CLIENT_EVENTS } from '@walletconnect/client';

const METADATA = {
    name: "StellarNFT",
    description: "Digital marketplace for non-fungible tokens (NFTs) on Stellar blockchain",
    url: "https://stellarnft.com/",
    icons: ["https://stellarnft.com/images/logo.svg"]
};

const JSONRPC_METHODS = {
    SIGN: 'stellar_signXDR',
    SIGN_SUBMIT: 'stellar_signAndSubmitXDR'
};
const TESTNET = 'stellar:testnet';
const PUBNET = 'stellar:pubnet';

export default class WalletConnectApi {

    constructor(events) {
        this.events = events;
        this.client = null;
        this.session = null;
    }

    async initWalletConnect() {
        if (this.client) {
            return null;
        }

        this.client = await WalletConnectClient.init({
            //logger: 'debug',
            relayProvider: 'wss://relay.walletconnect.org',
        });

        await new Promise(resolve => { setTimeout(() => resolve(), 500); });

        if(this.events && this.events.created) {
            this.client.on(
                CLIENT_EVENTS.pairing.created, 
                this.events.created
            );    
        }

        if(this.events && this.events.updated) {
            this.client.on(
                CLIENT_EVENTS.pairing.updated, 
                this.events.updated
            );    
        }
        
        if(this.events && this.events.deleted) {
            this.client.on(
                CLIENT_EVENTS.pairing.deleted, 
                this.events.deleted
            );    
        }

        if(this.events && this.events.proposal) {
            this.client.on(
                CLIENT_EVENTS.pairing.proposal, 
                this.events.proposal
            );    
        }

        if (!this.client.session.topics.length) {
            return null;
        }

        this.session =
            await this.client.session.get(this.client.session.topics[0]);
        
        return 'logged';
    }

    async login() {
        const result = await this.initWalletConnect();

        if (result === 'logged') {
            return;
        }

        this.session = await this.client.connect({
            metadata: METADATA,
            pairing: undefined,
            permissions: {
                blockchain: {
                    chains: [PUBNET],
                },
                jsonrpc: {
                    methods: [JSONRPC_METHODS.SIGN, JSONRPC_METHODS.SIGN_SUBMIT],
                },
            },
        });

        return 'ok';
    }

    async logout() {
        await this.initWalletConnect();
        
        if (this.session) {
            await this.client.disconnect({
                topic: this.session.topic,
                reason: 'log out',
            });
        }

        console.log('log out')
    }

    async getPublicKey() {
        await this.login();

        const [chain, reference, publicKey] = this.session.state.accounts[0].split(':');

        return publicKey;
    }

    async signTx(xdr, test) {
        await this.initWalletConnect();

        if (!this.client) {
            throw 'No Connection';
        }

        var signedXDR = false;

        await this.client.request({
            topic: this.session.topic,
            chainId: test ? TESTNET : PUBNET,
            request: {
                jsonrpc: '2.0',
                method: JSONRPC_METHODS.SIGN,
                params: {
                    xdr,
                },
            }
        }).then(result => {
            signedXDR = result.signedXDR;
        });

        if(!signedXDR) throw 'Error';

        return signedXDR;
    }

}