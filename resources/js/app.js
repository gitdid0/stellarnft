require('./bootstrap');

import { createApp, defineAsyncComponent } from 'vue'

const app = createApp({
    data () {
	    return {
            app: null,
            mobileMenuOpen: false,
            preloader: true
	    }
  	},
  	computed: {
	},
	methods: {
		toggleDarkMode() {
            var colorScheme = 'light'
            if(document.documentElement.classList.contains('dark')) {
                document.documentElement.classList.remove('dark')
                document.documentElement.classList.add('light')
            } else {
                document.documentElement.classList.remove('light')
                document.documentElement.classList.add('dark')
                colorScheme = 'dark'
            }

            axios.get('?colorScheme=' + colorScheme); 
        },
        scrollParentToChild(parent, child) {
            var parentRect = parent.getBoundingClientRect();
            var parentViewableArea = {
                height: parent.clientHeight,
                width: parent.clientWidth
            };
            var childRect = child.getBoundingClientRect();
            var isViewable = (childRect.right <= parentRect.width);

            if (!isViewable) {
                parent.scrollLeft = (childRect.right) - parentRect.width
            }
        },
        scrollToActiveTab() {
            var tabs = document.getElementsByClassName("tab-scroller")
            for (let i = 0; i < tabs.length; i++) {
                let active = tabs[i].getElementsByClassName("tab-active")[0];
                if(active) this.scrollParentToChild(tabs[i], active);
            }
        }
	},
  	mounted() {
        this.preloader = false;
		this.scrollToActiveTab();
    },
})

import VueEasyLightbox from 'vue-easy-lightbox'

app.use(VueEasyLightbox)

import Toast, { POSITION, useToast }  from "vue-toastification";

app.use(Toast, {
    position: POSITION.BOTTOM_RIGHT,
    timeout: 5000
});

window.toast = useToast();

// Components

app.component('dropdown', defineAsyncComponent(() => import('./components/Dropdown.vue')))

app.component('v-dropdown-list', defineAsyncComponent(() => import('./components/DropdownList.vue')))

app.component('v-img', defineAsyncComponent(() => import('./components/Img.vue')))

app.component('tw-modal',  defineAsyncComponent(() => import('./components/Modal.vue')))

app.component('countdown',  defineAsyncComponent(() => import('./components/Countdown.vue')))



// Assets

app.component('assets-item',  defineAsyncComponent(() => import('./components/Assets/Item.vue')))
app.component('assets-list',  defineAsyncComponent(() => import('./components/Assets/List.vue')))
app.component('assets-fields',  defineAsyncComponent(() => import('./components/Assets/AssetFields.vue')))
app.component('assets-add',  defineAsyncComponent(() => import('./components/Assets/Add.vue')))
app.component('assets-activate',  defineAsyncComponent(() => import('./components/Assets/Activate.vue')))
app.component('assets-update',  defineAsyncComponent(() => import('./components/Assets/Update.vue')))
app.component('assets-delete',  defineAsyncComponent(() => import('./components/Assets/Delete.vue')))
app.component('assets-claimable-balances',  defineAsyncComponent(() => import('./components/Assets/ClaimableBalances.vue')))

app.component('assets-share',  defineAsyncComponent(() => import('./components/Assets/Share.vue')))
app.component('assets-report',  defineAsyncComponent(() => import('./components/Assets/Report.vue')))

app.component('assets-detail-media',  defineAsyncComponent(() => import('./components/Assets/Detail/Media.vue')))

// Offers

app.component('offer-make',  defineAsyncComponent(() => import('./components/Assets/Offers/Make.vue')))
app.component('offer-cancel',  defineAsyncComponent(() => import('./components/Assets/Offers/Cancel.vue')))
app.component('offer-accept',  defineAsyncComponent(() => import('./components/Assets/Offers/Accept.vue')))

// Fields

app.component('v-input',  defineAsyncComponent(() => import('./components/Fields/Input.vue')))
app.component('v-textarea',  defineAsyncComponent(() => import('./components/Fields/Textarea.vue')))
app.component('v-file',  defineAsyncComponent(() => import('./components/Fields/File.vue')))
app.component('v-select',  defineAsyncComponent(() => import('./components/Fields/Select.vue')))
app.component('v-radio',  defineAsyncComponent(() => import('./components/Fields/Radio.vue')))
app.component('v-checkbox',  defineAsyncComponent(() => import('./components/Fields/Checkbox.vue')))
app.component('v-data-entries',  defineAsyncComponent(() => import('./components/Fields/DataEntries.vue')))
app.component('v-categories',  defineAsyncComponent(() => import('./components/Fields/Categories.vue')))

// NFT

app.component('nft-create',  defineAsyncComponent(() => import('./components/NFT/Create.vue')))
app.component('nft-show',  defineAsyncComponent(() => import('./components/NFT/Show.vue')))

// Auth

app.component('login',  defineAsyncComponent(() => import('./components/Auth/Login.vue')))

// Settings

app.component('settings-profile',  defineAsyncComponent(() => import('./components/Settings/Profile.vue')))

app.directive('click-outside', {
    beforeMount(el, binding, vnode) {
        el.clickOutsideEvent = function(event) {
            if (!(el === event.target || el.contains(event.target))) {
                binding.value(event, el);
            }
        };
        document.body.addEventListener('click', el.clickOutsideEvent);
    },
    unmounted(el) {
        document.body.removeEventListener('click', el.clickOutsideEvent);
    }
});

app.mount('#app')