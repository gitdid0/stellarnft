import albedo from '@albedo-link/intent'
import freighterApi from "@stellar/freighter-api";
import StellarBase from 'stellar-base';
import WalletConnectApi from "./Login/WalletConnect.js";

window.loginTransction = function(publicKey, sessionCode) {
    let account = new StellarBase.Account(publicKey, '0');

    let transaction = new StellarBase.TransactionBuilder(account, {
        fee: StellarBase.BASE_FEE,
        networkPassphrase: StellarBase.Networks.PUBLIC
    })
    .addOperation(StellarBase.Operation.manageData({
        name: sessionCode,
        value: '1',
        source: publicKey
    }))
    .setTimeout(180)
    .build();

    return transaction.toXDR();
}

window.createNFTTransction = async function(accountPublicKey, data, test) {
    const stroop = 0.0000001;
    const baseReserve = 0.5;
    const baseFee = 0.00001;
    const issuer = StellarBase.Keypair.random();
    let code = data.code;
    const asset = new StellarBase.Asset(code, issuer.publicKey());

    let operationsCount = 4;
    let baseReserveCount = 2;

    baseReserveCount += data.dataentries.length;

    if(data.name) baseReserveCount++;

    if(data.ipfs_cid) baseReserveCount++;

    if(data.royalty) baseReserveCount += 2;

    let issuerStartingBalance = (baseReserveCount * baseReserve).toFixed(7);

    let horizonServer = 'https://horizon.stellar.org/';
    let passphrase = StellarBase.Networks.PUBLIC;

    if(test) {
        horizonServer = 'https://horizon-testnet.stellar.org/';
        passphrase = StellarBase.Networks.TESTNET;

        await fetch('https://friendbot.stellar.org/?addr=' + accountPublicKey);
    }

    let addressSequence = false;
    await fetch( horizonServer + 'accounts/' + accountPublicKey)
    .then(response => response.json())
    .then(data => {
        if(data.sequence) {
            addressSequence = data.sequence
        }
    })

    if(!addressSequence) throw 'Your account does not exist on the ledger!';

    var account = new StellarBase.Account(accountPublicKey, addressSequence);

    var transaction = new StellarBase.TransactionBuilder(account, {
            fee: StellarBase.BASE_FEE,
            networkPassphrase: passphrase
        })
        .addOperation(StellarBase.Operation.createAccount({
            destination: issuer.publicKey(),
            startingBalance: issuerStartingBalance.toString()
        }))
        .addOperation(StellarBase.Operation.changeTrust({
            asset: asset
        }))
        .addOperation(StellarBase.Operation.payment({
            destination: accountPublicKey,
            asset: asset,
            amount: stroop.toString(),
            source: issuer.publicKey()
        }))
        .setTimeout(3600);

    data.dataentries.forEach((dataentry) => {
        transaction = transaction.addOperation(StellarBase.Operation.manageData({
            name: dataentry.name,
            value: dataentry.value,
            source: issuer.publicKey()
        }));
        operationsCount++;
    })

    if(data.name) {
        transaction = transaction.addOperation(StellarBase.Operation.manageData({
            name: 'name',
            value: data.name,
            source: issuer.publicKey()
        }));
        operationsCount++;
    }

    if(data.ipfs_cid) {
        transaction = transaction.addOperation(StellarBase.Operation.manageData({
            name: 'ipfs',
            value: data.ipfs_cid,
            source: issuer.publicKey()
        }));
        operationsCount++;
    }

    if(data.royalty) {
        transaction = transaction.addOperation(StellarBase.Operation.manageData({
            name: 'royaltyAmount',
            value: Math.round(data.royalty / stroop).toString(),
            source: issuer.publicKey()
        }));
        transaction = transaction.addOperation(StellarBase.Operation.manageData({
            name: 'royaltyReceiver',
            value: accountPublicKey,
            source: issuer.publicKey()
        }));
        operationsCount += 2;
    }

    let options = {
        masterWeight: 0,
        source: issuer.publicKey()
    }

    if(data.home_domain) {
        options.homeDomain = data.home_domain;
    }

    transaction = transaction.addOperation(StellarBase.Operation.setOptions(options));

    transaction = transaction.build();

    transaction.sign(StellarBase.Keypair.fromSecret(issuer.secret()));

    let xdr = transaction.toXDR();

    return {
        xdr: xdr,
        issuer: issuer.publicKey(),
        cost: (baseReserveCount * baseReserve) + (operationsCount * baseFee)
    }
}

window.getAccountBalance = async function(accountPublicKey, test) {
    let horizonServer = 'https://horizon.stellar.org/';

    if(test) {
        horizonServer = 'https://horizon-testnet.stellar.org/';
    }

    let accountBalance = false;

    await fetch( horizonServer + 'accounts/' + accountPublicKey)
    .then(response => response.json())
    .then(data => {
        if(data.balances) {
            let balances = data.balances;
            balances.forEach((balance) => {
                if(balance.asset_type == 'native') {
                    if(balance.balance) accountBalance = balance.balance;
                }
            })
        }
    })

    return accountBalance;
}

window.checkIfAccountExists = async function(accountPublicKey, test) {
    let horizonServer = 'https://horizon.stellar.org/';

    if(test) {
        horizonServer = 'https://horizon-testnet.stellar.org/';
    }

    let exists = false;

    await fetch( horizonServer + 'accounts/' + accountPublicKey)
    .then(response => response.json())
    .then(data => {
        if(data.sequence) {
            exists = true;
        }
    })

    return exists;
}

window.createAccountTransction = async function(accountPublicKey, destinationAccount, test) {
    const baseReserve = 0.5;

    let horizonServer = 'https://horizon.stellar.org/';
    let passphrase = StellarBase.Networks.PUBLIC;

    if(test) {
        horizonServer = 'https://horizon-testnet.stellar.org/';
        passphrase = StellarBase.Networks.TESTNET;
    }

    let addressSequence = false;
    await fetch( horizonServer + 'accounts/' + accountPublicKey)
    .then(response => response.json())
    .then(data => {
        if(data.sequence) {
            addressSequence = data.sequence
        }
    })

    if(!addressSequence) throw 'Your account does not exist on the ledger!';

    var account = new StellarBase.Account(accountPublicKey, addressSequence);

    var transaction = new StellarBase.TransactionBuilder(account, {
            fee: StellarBase.BASE_FEE,
            networkPassphrase: passphrase
        })
        .addOperation(StellarBase.Operation.createAccount({
            destination: destinationAccount,
            startingBalance: '1'
        }))
        .setTimeout(3600);

    transaction = transaction.build();

    let xdr = transaction.toXDR();

    return xdr;
}

window.createMergeTransction = async function(accountPublicKey, sourceAccount, test) {
    let horizonServer = 'https://horizon.stellar.org/';
    let passphrase = StellarBase.Networks.PUBLIC;

    if(test) {
        horizonServer = 'https://horizon-testnet.stellar.org/';
        passphrase = StellarBase.Networks.TESTNET;
    }

    let addressSequence = false;
    await fetch( horizonServer + 'accounts/' + accountPublicKey)
    .then(response => response.json())
    .then(data => {
        if(data.sequence) {
            addressSequence = data.sequence
        }
    })

    if(!addressSequence) throw 'Your account does not exist on the ledger!';

    var account = new StellarBase.Account(accountPublicKey, addressSequence);

    var transaction = new StellarBase.TransactionBuilder(account, {
            fee: StellarBase.BASE_FEE,
            networkPassphrase: passphrase
        })
        .addOperation(StellarBase.Operation.accountMerge({
            destination: accountPublicKey,
            source: sourceAccount
        }))
        .setTimeout(3600);

    transaction = transaction.build();

    let xdr = transaction.toXDR();

    return xdr;
}

window.getAccountNFTs = async function(publicKey, test) {
    let list = [];

    let horizonServer = 'https://horizon.stellar.org/';

    if(test) {
        horizonServer = 'https://horizon-testnet.stellar.org/';
    }

    await fetch( horizonServer + 'accounts/' + publicKey)
    .then(response => response.json())
    .then(data => {
        let balances = data.balances;
        balances.forEach((asset) => {
            if(asset.balance == '0.0000001' && asset.asset_issuer) {
                list.push({
                    'text': `${asset.asset_code} (${asset.asset_issuer.substr(0, 5)}...${asset.asset_issuer.substr(-5)})`,
                    'code': asset.asset_code,
                    'issuer': asset.asset_issuer,
                    'value': asset.asset_issuer
                })
            }
        })
    })

    return list;
}

window.getAccountClaimables = async function(publicKey, test) {
    let list = [];

    let horizonServer = 'https://horizon.stellar.org/';

    if(test) {
        horizonServer = 'https://horizon-testnet.stellar.org/';
    }

    await fetch( horizonServer + 'claimable_balances?claimant=' + publicKey + '&limit=100')
    .then(response => response.json())
    .then(data => {
        let claimables = data['_embedded']['records'];
        claimables.forEach((claimable) => {
            if(claimable.asset == 'native') {
                let claimants = claimable.claimants;
                let isValid = false;
                claimants.forEach((claimant) => {
                    if(claimant.destination == publicKey) {
                        if(claimant.predicate.unconditional) {
                            isValid = true;
                        } else if(claimant.predicate.not && claimant.predicate.not.abs_before) {
                            let current = Date.now();
                            let before = Date.parse(claimant.predicate.not.abs_before);
                            if(current >= before) isValid = true;
                        } 
                    } 
                })
                if(isValid) {
                    list.push({
                        'id': claimable.id,
                        'from': claimable.sponsor,
                        'amount': claimable.amount,
                        'created_at': claimable.last_modified_time
                    })
                }    
            }
        })
    })

    return list;
}

window.createClaimClaimableBalancesTransaction = async function(publicKey, claimables, test) {
    const baseReserve = 0.5;

    let horizonServer = 'https://horizon.stellar.org/';
    let passphrase = StellarBase.Networks.PUBLIC;

    if(test) {
        horizonServer = 'https://horizon-testnet.stellar.org/';
        passphrase = StellarBase.Networks.TESTNET;
    }

    let addressSequence = false;
    await fetch( horizonServer + 'accounts/' + publicKey)
    .then(response => response.json())
    .then(data => {
        if(data.sequence) {
            addressSequence = data.sequence
        }
    })

    if(!addressSequence) throw 'Your account does not exist on the ledger!';

    var account = new StellarBase.Account(publicKey, addressSequence);

    var transaction = new StellarBase.TransactionBuilder(account, {
            fee: StellarBase.BASE_FEE,
            networkPassphrase: passphrase
        });

    claimables.forEach((claimable) => {
        transaction = transaction.addOperation(StellarBase.Operation.claimClaimableBalance({
            balanceId: claimable.id
        }))
    })

    transaction = transaction.setTimeout(3600).build();

    let xdr = transaction.toXDR();

    return xdr;
}


window.isConnectedWallet = function(wallet) {
    if(wallet == 'albedo') {
        return true;
    }

    if(wallet == 'walletconnect') {
        return true;
    }

    if(wallet == 'freighter') {
        return freighterApi.isConnected();
    }

    if(wallet == 'rabet') {
        return window.rabet ? true : false
    }

    throw 'Error has occurred';
}

window.getPublicKey = async function(wallet) {
    let pubKey = false

    if(wallet == 'albedo') {
        await albedo.publicKey().then(res => {
            pubKey = res.pubkey;
        }).catch((error) => {
            throw 'Error has occurred';
        });
    }

    if(wallet == 'walletconnect') {
        
        var walletconnect = new WalletConnectApi;

        var publicKey = await walletconnect.getPublicKey();

        pubKey = publicKey;
    }

    if(wallet == 'freighter') {
        await freighterApi.getPublicKey().then(pubkey => {
            pubKey = pubkey;
        })
        .catch(error => {
            throw 'Error has occurred';
        });
    }

    if(wallet == 'rabet') {
        await rabet.connect().then(result => {
            pubKey = result.publicKey;
        })
        .catch(error => {
            throw 'Error has occurred';
        });
    }

    if(pubKey) return pubKey;

    throw 'Error has occurred';
}

window.reconnectWallet = async function(wallet) {
    
    if(wallet == 'walletconnect') {
        
        var walletconnect = new WalletConnectApi;

        await walletconnect.logout();

    }

    return true;
}

window.logOutFromWallet = async function(wallet) {
    
    if(wallet == 'walletconnect') {
        
        var walletconnect = new WalletConnectApi;

        await walletconnect.logout();

    }

    return true;
}


window.signTransction = async function(wallet, xdr, test) {
    let transactionXDR = false;

    if(wallet == 'walletconnect') {
        
        var walletconnect = new WalletConnectApi;

        await walletconnect.signTx(xdr, test)
        .then((res) => {
            transactionXDR = res;
        }).catch((error) => {
            throw 'Error has occurred';
        })

    }
    
    if(wallet == 'albedo') {
        await albedo.tx({
            xdr: xdr,
            network: test ? 'testnet' : 'public',
            submit: false
        }).then((res) => {
            transactionXDR = res.signed_envelope_xdr;
        }).catch((error) => {
            throw 'Error has occurred';
        })
    }

    if(wallet == 'freighter') {
        await freighterApi.signTransaction(xdr, (test ? 'TESTNET' : 'PUBLIC')).then((userSignedTransaction) => {
            transactionXDR = userSignedTransaction;
        }).catch((error) => {
            throw 'Error has occurred';
        })
    }

    if(wallet == 'rabet') {
        await rabet.sign(xdr, test ? 'testnet' : 'mainnet').then((result) => {
            transactionXDR = result.xdr
        }).catch((error) => {
            throw 'Error has occurred';
        })
    }

    if(transactionXDR) return transactionXDR;

    throw 'Error has occurred';
}

window.submitTransaction = async function (xdr, test) {
    let status = false;

    let error = 'Error has occurred';

    const tx = encodeURIComponent(xdr);

    await fetch(`https://horizon${test ? '-testnet' : ''}.stellar.org/transactions/`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `tx=${tx}`
    })
    .then(response => response.json())
    .then(data => {
        if(data.successful) {
            status = true;
        }
        if(data.status && data.status == '400') {
            if(data.extras && data.extras.result_codes && data.extras.result_codes.operations) {
                var operationsFaild = data.extras.result_codes.operations;
                if(operationsFaild[0] && operationsFaild[0] == 'op_underfunded')  error = 'Insufficient balance';
            }
            if(data.extras && data.extras.result_codes && data.extras.result_codes.transaction) {
                if(data.extras.result_codes.transaction == 'tx_bad_auth')  error = 'Wrong signature';
            }
        }
    }).catch((data) => {
        throw error;
    });

    if(status) return status;

    throw error;
}