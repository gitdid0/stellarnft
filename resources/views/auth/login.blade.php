@extends('layouts.app')

@section('title', __('Sign in').' | '.config('app.name'))

@section('content')
    <div class="py-6 sm:max-w-5xl mx-auto">
        <div class="hero-headline px-4">
            <div class="text-center">
                <h1 class="text-3xl tracking-tight font-thin text-purple-500">
                    <div class="block">{{ __('Sign in to StellarNFT') }}</div>
                </h1>
            </div>
        </div>

        <login backurl="{{ getPrevUrl() }}" :wallet-list='@json(config("site.wallets"))' action="{{ route('login.store') }}" session-code="{{ $sessionCode }}"></login>
    </div>
@endsection