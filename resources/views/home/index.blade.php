@extends('layouts.app')

@section('title', __('Buy, Sell and Explore NFTs on Stellar blockchain').' | '.config('app.name'))

@section('meta')
<meta name="description" content="Digital marketplace for non-fungible tokens (NFTs) on Stellar blockchain" />
<meta property="og:description" content="Digital marketplace for non-fungible tokens (NFTs) on Stellar blockchain" />
@endsection

@section('content')
    <div class="container mx-auto sm:px-6 lg:px-8">
        <main class="mt-10 mx-auto max-w-7xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
            <div class="sm:text-center">
                <div class="sm:flex justify-center">
                    <svg class="h-8 sm:h-12" fill="currentColor" viewBox="0 0 976.7 180.7">
                        <use xlink:href="/images/logo.svg#logo"></use>
                    </svg>
                </div>

                <p class="mt-3 text-base text-gray-500 sm:mt-5 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-5 md:text-xl">
                    Digital marketplace for non-fungible tokens (NFTs) on Stellar blockchain
                </p>
                <div class="mt-5 sm:mt-8 sm:flex sm:justify-center">
                    <div class="rounded-md shadow">
                        <a href="{{ route('assets.index') }}" class="w-full flex items-center justify-center px-6 py-2 border border-transparent text-base font-medium rounded-md text-white bg-purple-500 hover:bg-purple-600">
                            Browse
                        </a>
                    </div>
                    <div class="mt-3 sm:mt-0 sm:ml-3">
                        <a href="{{ route('assets.add') }}" class="w-full flex items-center justify-center px-6 py-2 border border-transparent text-base font-medium rounded-md text-purple-700 bg-purple-100 hover:bg-purple-200">
                            Add Item
                        </a>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection