@component('mail::message')

Your offer for {{ $asset->version->name }} have been accepted!

@component('mail::button', ['url' => $asset->url])
View Item
@endcomponent

{{ config('app.name') }}
@endcomponent