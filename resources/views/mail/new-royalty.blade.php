@component('mail::message')

You received royalty revenue!

@component('mail::button', ['url' => route('account.claimable-balances-redirect')])
Claim It
@endcomponent

{{ config('app.name') }}
@endcomponent