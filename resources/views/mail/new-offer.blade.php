@component('mail::message')

You received a new offer for {{ $asset->version->name }}!

@component('mail::button', ['url' => $asset->url])
View Item
@endcomponent

{{ config('app.name') }}
@endcomponent