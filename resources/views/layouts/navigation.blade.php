<nav class="bg-white border-b shadow border-gray-100 dark:bg-gray-700 dark:border-gray-700">
    <div class="container mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">
                <div class="flex-shrink-0 flex items-center">
                    <a href="/">
                        <div class="flex items-center text-balck dark:text-white">
                            <svg class="h-5 sm:h-6" fill="currentColor" viewBox="0 0 976.7 180.7">
                                <use xlink:href="/images/logo.svg#logo"></use>
                            </svg> 
                        </div>
                    </a>
                </div>

                <div class="hidden space-x-8 lg:-my-px lg:ml-10 lg:flex text-sm">
                    <x-nav-link :href="route('assets.index')" :active="request()->routeIs('assets.index*')">
                        {{ __('Browse') }}
                    </x-nav-link>
                    @if(session('auth', false))
                    <x-nav-link :href="route('account.items', session('publicKey'))" :active="false">
                        {{ __('My Items') }}
                    </x-nav-link>
                    <x-nav-link :href="route('account.offers-made', session('publicKey'))" :active="false">
                        {{ __('My offers') }}
                    </x-nav-link>
                    <x-nav-link :href="route('account.claimable-balances', session('publicKey'))" :active="false">
                        {{ __('Claimable Balances') }}
                    </x-nav-link>
                    @endif
                    <x-nav-link :href="route('pages.help')" :active="request()->routeIs('pages.help*')">
                        {{ __('Help') }}
                    </x-nav-link>
                </div>
            </div>

            <div class="hidden lg:flex lg:items-center lg:ml-6 space-x-4">
                @if(session('auth', false))
                <x-dropdown align="right" width="48">
                    <x-slot name="trigger">
                        <button class="bg-gray-100 dark:bg-gray-800 p-2 rounded-full text-purple-500 hover:bg-gray-200 dark:hover:bg-gray-600 transition duration-150 ease-in-out focus:outline-none">
                            <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                            </svg>
                        </button>
                    </x-slot>

                    <x-slot name="content">
                        <div class="px-4 py-2">
                            <div class="font-medium text-base break-all">{{ truncStellarAddress(session('publicKey')) }}</div>
                        </div>
                        <x-dropdown-link :href="route('account.show', session('publicKey'))">
                            {{ __('My Profile') }}
                        </x-dropdown-link>
                        <x-dropdown-link :href="route('assets.add')">
                            {{ __('Add New Item') }}
                        </x-dropdown-link>
                        <x-dropdown-link :href="route('nfts.create')">
                            {{ __('Create New NFT') }}
                        </x-dropdown-link>
                        <x-dropdown-link :href="route('settings.index')">
                            {{ __('Account Settings') }}
                        </x-dropdown-link>
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <x-dropdown-link :href="route('logout')"
                                    onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                {{ __('Logout') }}
                            </x-dropdown-link>
                        </form>
                    </x-slot>
                </x-dropdown>
                @else
                <x-button :href="route('login')">
                    {{ __('Login') }}
                </x-button>
                @endif
            </div>

            <div class="-mr-2 flex items-center lg:hidden">
                <button @click="mobileMenuOpen = ! mobileMenuOpen" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 focus:outline-none focus:text-gray-500 transition duration-150 ease-in-out">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path :class="{'hidden': mobileMenuOpen, 'inline-flex': ! mobileMenuOpen }" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                        <path :class="{'hidden': ! mobileMenuOpen, 'inline-flex': mobileMenuOpen }" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <div :class="{'block': mobileMenuOpen, 'hidden': ! mobileMenuOpen}" class="lg:hidden">
        <div class="pt-2 pb-3 space-y-1">
            <x-responsive-nav-link :href="route('assets.index')" :active="request()->routeIs('assets.index*')">
                {{ __('Browse') }}
            </x-responsive-nav-link>
            @if(session('auth', false))
            <x-responsive-nav-link :href="route('account.items', session('publicKey'))" :active="false">
                {{ __('My Items') }}
            </x-responsive-nav-link>
            <x-responsive-nav-link :href="route('account.offers-made', session('publicKey'))" :active="false">
                {{ __('My offers') }}
            </x-responsive-nav-link>
            <x-responsive-nav-link :href="route('account.claimable-balances', session('publicKey'))" :active="false">
                {{ __('Claimable Balances') }}
            </x-responsive-nav-link>
            @endif
            <x-responsive-nav-link :href="route('pages.help')" :active="request()->routeIs('pages.help*')">
                {{ __('Help') }}
            </x-responsive-nav-link>
        </div>

        <div class="pt-4 pb-1 border-t border-gray-200 dark:border-gray-600">

            @if(session('auth'))
            <div class="px-4">
                <div class="font-medium text-purple-500 text-base break-all">{{ truncStellarAddress(session('publicKey')) }}</div>
            </div>
            <div class="mt-3 space-y-1">
                <x-responsive-nav-link :href="route('account.show', session('publicKey'))" :active="request()->routeIs('account.show*')">
                    {{ __('My Profile') }}
                </x-responsive-nav-link>
                <x-responsive-nav-link :href="route('assets.add')" :active="request()->routeIs('assets.add*')">
                    {{ __('Add New Item') }}
                </x-responsive-nav-link>
                <x-responsive-nav-link :href="route('nfts.create')" :active="request()->routeIs('nfts.create*')">
                    {{ __('Create New NFT') }}
                </x-responsive-nav-link>
                <x-responsive-nav-link :href="route('settings.index')" :active="request()->routeIs('settings*')">
                    {{ __('Account Settings') }}
                </x-responsive-nav-link>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <x-responsive-nav-link :href="route('logout')"
                            onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Logout') }}
                    </x-responsive-nav-link>
                </form>
            </div>
            @else
            <x-responsive-nav-link :href="route('login')">
                {{ __('Login') }}
            </x-responsive-nav-link>
            @endif
        </div>
    </div>
</nav>
