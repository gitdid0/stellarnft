<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Metas -->
        @section('meta')
        @show

        <title>@yield('title', 'StellarNFT')</title>

        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <style>
            #preloader{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;position:fixed;width:100%;height:100vh;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;z-index:2147483647!important;background:#f3f4f6}#preloader .lds-grid{display:inline-block;position:relative;width:120px;height:120px}#preloader .lds-grid div{position:absolute;width:24px;height:24px;-webkit-border-radius:50%;border-radius:50%;background:#8b5cf6;-webkit-animation:lds-grid 1.2s linear infinite;animation:lds-grid 1.2s linear infinite}#preloader .lds-grid div:nth-child(1){top:12px;left:12px;-webkit-animation-delay:0s;animation-delay:0s}#preloader .lds-grid div:nth-child(2){top:12px;left:48px;-webkit-animation-delay:-.4s;animation-delay:-.4s}#preloader .lds-grid div:nth-child(3){top:12px;left:84px;-webkit-animation-delay:-.8s;animation-delay:-.8s}#preloader .lds-grid div:nth-child(4){top:48px;left:12px;-webkit-animation-delay:-.4s;animation-delay:-.4s}#preloader .lds-grid div:nth-child(5){top:48px;left:48px;-webkit-animation-delay:-.8s;animation-delay:-.8s}#preloader .lds-grid div:nth-child(6){top:48px;left:84px;-webkit-animation-delay:-1.2s;animation-delay:-1.2s}#preloader .lds-grid div:nth-child(7){top:84px;left:12px;-webkit-animation-delay:-.8s;animation-delay:-.8s}#preloader .lds-grid div:nth-child(8){top:84px;left:48px;-webkit-animation-delay:-1.2s;animation-delay:-1.2s}#preloader .lds-grid div:nth-child(9){top:84px;left:84px;-webkit-animation-delay:-1.6s;animation-delay:-1.6s}@-webkit-keyframes lds-grid{0%,100%{opacity:1}50%{opacity:.5}}@keyframes lds-grid{0%,100%{opacity:1}50%{opacity:.5}}
        </style>

    </head>
    <body class="@yield('body-class') font-roboto bg-gray-100 dark:bg-gray-800 text-gray-600 dark:text-gray-200">
        <div id="app" class="flex flex-col h-screen">

            <transition
                enter-active-class="transition ease-out duration-300 transform"
                enter-class="opacity-0"
                enter-to-class="opacity-100"
                leave-active-class="transition ease-in duration-200 transform"
                leave-class="opacity-100"
                leave-to-class="opacity-0"
            >
                <div id="preloader" v-if="preloader">
                    <div class="lds-grid"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                </div>
            </transition>

            @include('layouts.header')

            @if (session('warning'))
                <div class="text-yellow-600 bg-yellow-200 text-center text-sm font-bold px-4 py-3" role="alert">
                    <p>{!! session('warning') !!}</p>
                </div>
            @endif

            @if (session('danger'))
                <div class="text-red-600 bg-red-200 text-center text-sm font-bold px-4 py-3" role="alert">
                    <p>{!! session('danger') !!}</p>
                </div>
            @endif

            @if (session('success'))
                <div class="text-green-600 bg-green-200 text-center text-sm font-bold px-4 py-3" role="alert">
                    <p>{!! session('success') !!}</p>
                </div>
            @endif

            @yield('before_content')

            <main class="@yield('main-class') pb-6" id="@yield('main-id')">
                @yield('content')
            </main>

            @include('layouts.footer')
        </div>
        <div id="modals"></div>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <link rel="stylesheet" href="{{ mix('css/toast.css') }}">

        <link rel="stylesheet" href="{{ mix('css/plyr.css') }}" />

        @yield('styles')

        <!-- Scripts -->
        @yield('scripts')
        <script src="{{ mix('js/wallet-helper.js') }}" defer></script>
        <script src="{{ mix('js/app.js') }}" defer></script>
        <script src="{{ mix('js/plyr.js') }}" defer></script>

        @if (App::environment('production'))
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-RS8HD2WL3C"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-RS8HD2WL3C');
        </script>
        @endif
    </body>
</html>
