@extends('layouts.app')

@section('title', 'NFTs created by '.truncStellarAddress($address).' | '.config('app.name'))

@section('meta')
<meta name="robots" content="noindex">
@endsection

@section('content')
    <x-profile.header :address="$address"></x-profile.header>

    <div class="container mx-auto sm:px-6 lg:px-8 py-6">
        <assets-list :show-bounty-info="{{ \App\Helpers\BountyHelper::showBountyInfo() ? 'true' : 'false' }}" action="{{ route('account.fetch-created', $address) }}">
            <template #no-items>
                <div class="text-2xl font-thin">No items found</div>
            </template>
        </assets-list>
    </div>
@endsection