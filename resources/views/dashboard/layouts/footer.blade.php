<footer class="footer bg-white dark:bg-gray-700 mt-auto py-6 px-4 sm:px-0">
    <div class="sm:px-6 lg:px-8">
        <div class="sm:flex flex-wrap items-center justify-between text-gray-400">
            <div class="space-y-2 sm:space-y-0 sm:flex items-center sm:divide-x devide-gray-300 sm:-mx-3">
                <div class="sm:px-3"><a href="{{ route('home') }}" class="hover:text-purple-500">Back to Main Site</a></div>
            </div>
            <div class="text-left sm:text-right">
                <div class="mt-2 sm:mt-0 copyright text-gray-400">© {{ date('Y') }} Stellar<span class="text-purple-500">NFT</span></div>
            </div>
        </div>
    </div>
</footer>