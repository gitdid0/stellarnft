
<header id="header" class="z-50">
    <nav class="bg-white border-b shadow border-gray-100 dark:bg-gray-700 dark:border-gray-700">
        <div class="px-4 sm:px-6 lg:px-8">
            <div class="flex justify-between h-16">
                <div class="-ml-2 flex items-center lg:hidden">
                    <button @click="mobileMenuOpen = ! mobileMenuOpen" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 focus:outline-none focus:text-gray-500 transition duration-150 ease-in-out">
                        <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                            <path :class="{'hidden': mobileMenuOpen, 'inline-flex': ! mobileMenuOpen }" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                            <path :class="{'hidden': ! mobileMenuOpen, 'inline-flex': mobileMenuOpen }" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                        </svg>
                    </button>
                </div>

                <div class="flex">
                    <div class="flex-shrink-0 flex items-center">
                        <a href="/">
                            <div class="flex items-center text-balck dark:text-white">
                                <svg class="h-5 sm:h-6" fill="currentColor" viewBox="0 0 976.7 180.7">
                                    <use xlink:href="/images/logo.svg#logo"></use>
                                </svg> 
                            </div>
                        </a>
                    </div>

                    <div class="hidden space-x-8 lg:-my-px lg:ml-10 lg:flex text-sm">
                    </div>
                </div>

                <div class="flex items-center ml-6 space-x-4">
                    <x-button :href="route('home')">
                        {{ __('Main Site') }}
                    </x-button>
                </div>
            </div>
        </div>
    </nav>
</header>