@if ($paginator->hasPages())
    <nav role="navigation" aria-label="{{ __('Pagination Navigation') }}" class="flex items-center justify-between py-3">
        <div class="flex justify-between flex-1 sm:hidden">
            @if ($paginator->onFirstPage())
                <span class="opacity-40 h-10 px-2 rounded-sm focus:outline-none border border-gray-100 dark:border-gray-600 flex items-center justify-center">
                    {!! __('pagination.previous') !!}
                </span>
            @else
                <a href="{{ $paginator->previousPageUrl() }}" class="h-10 px-2 rounded-sm focus:outline-none border border-gray-100 hover:border-purple-500 dark:border-gray-600 dark:hover:border-purple-500 flex items-center justify-center hover:bg-purple-500 hover:text-white dark:hover:bg-purple-500">
                    {!! __('pagination.previous') !!}
                </a>
            @endif

            @if ($paginator->hasMorePages())
                <a href="{{ $paginator->nextPageUrl() }}" class="h-10 px-2 rounded-sm focus:outline-none border border-gray-100 hover:border-purple-500 dark:border-gray-600 dark:hover:border-purple-500 flex items-center justify-center hover:bg-purple-500 hover:text-white dark:hover:bg-purple-500">
                    {!! __('pagination.next') !!}
                </a>
            @else
                <span class="opacity-40 h-10 px-2 rounded-sm focus:outline-none border border-gray-100 dark:border-gray-600 flex items-center justify-center">
                    {!! __('pagination.next') !!}
                </span>
            @endif
        </div>

        <div class="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
            <div>
                <p class="text-sm leading-5">
                    {!! __('Showing') !!}
                    <span class="font-medium">{{ $paginator->firstItem() }}</span>
                    {!! __('to') !!}
                    <span class="font-medium">{{ $paginator->lastItem() }}</span>
                    {!! __('of') !!}
                    <span class="font-medium">{{ $paginator->total() }}</span>
                    {!! __('results') !!}
                </p>
            </div>

            <div>
                <ul class="z-0 inline-flex bg-gray-100 dark:bg-gray-600 shadow-sm space-x-1">
                    {{-- Previous Page Link --}}
                    @if (!$paginator->onFirstPage())
                    <li class="rounded-sm focus:outline-none border border-gray-100 table-cell hover:border-purple-500 dark:border-gray-600 dark:hover:border-purple-500">
                        <a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="flex items-center justify-center focus:outline-none hover:bg-purple-500 hover:text-white w-8 h-8 dark:hover:bg-purple-500" aria-label="{{ __('pagination.previous') }}">
                            <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                            </svg>
                        </a>
                    </li>
                    @endif

                    {{-- Pagination Elements --}}
                    @foreach ($elements as $element)
                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                            <li class="rounded-sm focus:outline-none border table-cell border-gray-100 dark:border-gray-600">
                                <span class="flex items-center justify-center w-8 h-8">{{ $element }}</span>
                            </li>
                        @endif

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <li class="rounded-sm focus:outline-none border table-cell border-purple-500 dark:border-purple-500">
                                        <span class="flex items-center justify-center focus:outline-none bg-purple-500 w-8 h-8 text-white dark:bg-purple-500">{{ $page }}</span>
                                    </li>
                                @else
                                    <li class="rounded-sm focus:outline-none border border-gray-100 table-cell hover:border-purple-500 dark:border-gray-600 dark:hover:border-purple-500">
                                        <a href="{{ $url }}"  class="flex items-center justify-center focus:outline-none hover:bg-purple-500 hover:text-white w-8 h-8 dark:hover:bg-purple-500">{{ $page }}</a>
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    @endforeach

                    {{-- Next Page Link --}}
                    @if ($paginator->hasMorePages())
                        <li class="rounded-sm focus:outline-none border border-gray-100 table-cell hover:border-purple-500 dark:border-gray-600 dark:hover:border-purple-500">
                            <a href="{{ $paginator->nextPageUrl() }}" rel="next" class="flex items-center justify-center focus:outline-none hover:bg-purple-500 hover:text-white w-8 h-8 dark:hover:bg-purple-500" aria-label="{{ __('pagination.previous') }}">
                                <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                </svg>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
@endif
