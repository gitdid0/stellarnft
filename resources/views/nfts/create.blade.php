@extends('layouts.app')

@section('title', __('Create New NFT').' | '.config('app.name'))

@section('content')
    <div class="py-6 sm:max-w-5xl mx-auto">
        <div class="hero-headline px-4 sm:text-center">
            <div class="sm:text-center">
                <h1 class="text-4xl tracking-tight font-thin text-purple-500 sm:text-5xl md:text-6xl">
                    <div class="block">Create New NFT</div>
                </h1>
            </div>
        </div>

        <nft-create :test="{{ App::environment('production') ? 'false' : 'true' }}" validate-action="{{ route('nfts.validate') }}" wallet="{{ session('wallet') }}" public-key="{{ session('publicKey') }}"></nft-create>
        
    </div>
@endsection