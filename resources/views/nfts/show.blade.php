@extends('layouts.app')

@section('title',  $code.' | '.config('app.name'))

@section('content')
    <div class="py-6 sm:max-w-3xl mx-auto">
        <div class="hero-headline px-4 sm:text-center">
            <div class="sm:text-center">
                <h1 class="text-4xl tracking-tight font-thin text-purple-500 sm:text-5xl md:text-6xl">
                    <div class="block">{{ $code }}</div>
                </h1>
            </div>
        </div>

        <nft-show code="{{ $code }}" issuer="{{ $issuer }}"></nft-show>
    </div>
@endsection