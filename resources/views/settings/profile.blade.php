@extends('layouts.app')

@section('title', 'Profile Settings | '.config('app.name'))


@section('content')
    <div class="container mx-auto sm:px-6 lg:px-8 py-6">
        <div class="hero-headline px-4 sm:text-center">
            <h1 class="text-4xl tracking-tight font-thin sm:text-5xl md:text-6xl text-purple-500">Settings</h1>
            <p class="mt-3 text-sm text-gray-400 sm:max-w-xl sm:mx-auto">Profile Settings</p>
        </div>

        <div class="mt-8 sm:mt-12">

            <div class="md:grid md:grid-cols-12 md:gap-6">
                <div class="col-span-3">
                    @include('settings.navigation')
                </div>

                <div class="col-span-9">
                    <settings-profile verification-url="{{ route('pages.verification') }}" :verified="{{ \App\Helpers\MarketplaceHelper::checkVerified(session('publicKey')) ? 'true' : 'false' }}" :preset-data='@json($profileData)' action="{{ route('settings.profile.set') }}"></settings-profile>
                </div>
            </div>
            
        </div>
    </div>
@endsection