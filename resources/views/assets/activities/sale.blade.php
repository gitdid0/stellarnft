<tr class="bg-gray-50">
    <td class="px-3 py-4 whitespace-nowrap">
        {{ $activity->type::getName() }}
    </td>
    <td class="px-3 py-4 whitespace-nowrap">
        @if(!empty($activity->data['price']))
        <div class="flex items-center">
            <div class="tooltip tooltip-top mr-1" data-tooltip="XLM">
                <svg xmlns="http://www.w3.org/2000/svg" class="flex-none h-4 w-4" fill="currentColor" viewBox="0 0 236.36 200" xml:space="preserve">
                    <path d="M203,26.16l-28.46,14.5-137.43,70a82.49,82.49,0,0,1-.7-10.69A81.87,81.87,0,0,1,158.2,28.6l16.29-8.3,2.43-1.24A100,100,0,0,0,18.18,100q0,3.82.29,7.61a18.19,18.19,0,0,1-9.88,17.58L0,129.57V150l25.29-12.89,0,0,8.19-4.18,8.07-4.11v0L186.43,55l16.28-8.29,33.65-17.15V9.14Z"/>
                    <path d="M236.36,50,49.78,145,33.5,153.31,0,170.38v20.41l33.27-16.95,28.46-14.5L199.3,89.24A83.45,83.45,0,0,1,200,100,81.87,81.87,0,0,1,78.09,171.36l-1,.53-17.66,9A100,100,0,0,0,218.18,100c0-2.57-.1-5.14-.29-7.68a18.2,18.2,0,0,1,9.87-17.58l8.6-4.38Z"/>
                </svg>
            </div>
            <span> {{ !empty($activity->data['price']) ? formatPrice($activity->data['price']) : '0' }}</span>
        </div>
        @endif
    </td>
    <td class="px-3 py-4 whitespace-nowrap">
        @if(!empty($activity->data['from']))
        <a class="text-purple-500" target="new-activity-from"  href="{{ route('account.items', $activity->data['from']) }}">{{ truncStellarAddress($activity->data['from']) }}</a>
        @endif
    </td>
    <td class="px-3 py-4 whitespace-nowrap">
        @if(!empty($activity->data['to']))
        <a class="text-purple-500" target="new-activity-from"  href="{{ route('account.items', $activity->data['to']) }}">{{ truncStellarAddress($activity->data['to']) }}</a>
        @endif
    </td>
    <td class="px-3 py-4 whitespace-nowrap">
        @if(!empty($activity->data['tx']))
        <a class="text-purple-500" target="new-activity-date"  href="https://stellar.expert/explorer/public/tx/{{ $activity->data['tx'] }}">
            {{ $activity->created_at->diffForHumans() }}
        </a>
        @else
            {{ $activity->created_at->diffForHumans() }}
        @endif
    </td>
</tr>