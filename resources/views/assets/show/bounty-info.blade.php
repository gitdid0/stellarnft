<v-dropdown-list :default-state="true" name="asset-unlockable-content" class="mt-6 bg-white dark:bg-gray-700 border border-gray-300 dark:border-gray-400 sm:rounded overflow-hidden">
    <template #header>
        <div class="flex items-center">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2 text-purple-500" fill="currentColor" viewBox="0 0 576 512" xml:space="preserve">
                <path d="M448 32H128C57.31 32 0 89.31 0 160v288c0 17.67 14.33 32 32 32h512c17.67 0 32-14.33 32-32V160c0-70.69-57.31-128-128-128zM96 432H48V288h48v144zm0-192H48v-80c0-32.72 19.8-60.84 48-73.22V240zm336 192H144V288h80v48c0 8.84 7.16 16 16 16h96c8.84 0 16-7.16 16-16v-48h80v144zM272 288v-32c0-8.84 7.16-16 16-16s16 7.16 16 16v32c0 8.84-7.16 16-16 16s-16-7.16-16-16zm160-48h-80v-32c0-8.84-7.16-16-16-16h-96c-8.84 0-16 7.16-16 16v32h-80V80h288v160zm96 192h-48V288h48v144zm0-192h-48V86.78c28.2 12.38 48 40.5 48 73.22v80z" class=""></path>
            </svg>
            <span class="font-medium tracking-tight">Bounty Information</span>
        </div>
    </template>
    <template #body>
        <div class="p-3">
            @if($asset->owner == session('publicKey'))
                <div>Bounty <span class="text-purple-500 font-medium">{{ $asset->version->bounty * config('site.stroop') }}%</span></div>
                <div class="mt-2">
                    <a href="{{ $asset->edit_url }}" class="text-purple-500 hover:underline font-medium"><span>Change Bounty Percentage</span></a>
                </div>
            @else
                @if($asset->version->bounty > 0)
                <div>Bounty <span class="text-purple-500 font-medium">{{ $asset->version->bounty * config('site.stroop') }}%</span> of the total sale price</div>
                <div class="mt-1">
                    <div class="flex items-center">
                        <div class="tooltip tooltip-right sm:tooltip-top mr-2" data-tooltip="XLM">
                            <svg xmlns="http://www.w3.org/2000/svg" class="flex-none h-6 w-6" fill="currentColor" viewBox="0 0 236.36 200" xml:space="preserve">
                                <path d="M203,26.16l-28.46,14.5-137.43,70a82.49,82.49,0,0,1-.7-10.69A81.87,81.87,0,0,1,158.2,28.6l16.29-8.3,2.43-1.24A100,100,0,0,0,18.18,100q0,3.82.29,7.61a18.19,18.19,0,0,1-9.88,17.58L0,129.57V150l25.29-12.89,0,0,8.19-4.18,8.07-4.11v0L186.43,55l16.28-8.29,33.65-17.15V9.14Z"/>
                                <path d="M236.36,50,49.78,145,33.5,153.31,0,170.38v20.41l33.27-16.95,28.46-14.5L199.3,89.24A83.45,83.45,0,0,1,200,100,81.87,81.87,0,0,1,78.09,171.36l-1,.53-17.66,9A100,100,0,0,0,218.18,100c0-2.57-.1-5.14-.29-7.68a18.2,18.2,0,0,1,9.87-17.58l8.6-4.38Z"/>
                            </svg>
                        </div>
                        <span class="text-2xl font-medium">
                            {{ \App\Helpers\BountyHelper::getBountyValue($asset->version->bounty, $asset->version->price, true) }}
                            <span class="text-sm font-normal text-gray-400">(${{ \App\Helpers\MarketplaceHelper::getXLMPrice(\App\Helpers\BountyHelper::getBountyValue($asset->version->bounty, $asset->version->price), true) }})</span>
                        </span>
                    </div>
                </div>
                <div class="mt-4">
                    <label for="referral_link" class="block font-medium text-sm">Your Referral Link</label>
                    <div class="mt-1 flex rounded-md shadow-sm">
                        <input type="text" value="{{ \App\Helpers\BountyHelper::getRferralLink($asset)  }}" name="referral_link" id="referral_linke" class="dark:bg-gray-800 focus:border-purple-500 focus:ring focus:ring-purple-500 focus:ring-opacity-50 flex-1 block w-full rounded-none z-10 rounded-md  border-gray-300 dark:border-gray-400" readonly>
                    </div>
                </div>
                @else
                <div>No bounty on this item.</div>
                @endif
            @endif
        </div>
    </template>
</v-dropdown-list>