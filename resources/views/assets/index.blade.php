@extends('layouts.app')

@section('title', 'Explore digital assets | '.config('app.name'))

@section('meta')
<meta name="description" content="Explore digital assets" />
<meta property="og:description" content="Explore digital assets" />
@endsection

@section('content')
    <div class="container mx-auto sm:px-6 lg:px-8 py-6">
        <div class="hero-headline px-4 sm:text-center">
            <h1 class="text-4xl tracking-tight font-thin sm:text-5xl md:text-6xl text-purple-500">Digital Assets</h1>
            <p class="mt-3 text-sm text-gray-400 sm:max-w-xl sm:mx-auto">Buy, Sell and Explore</p>
        </div>

        <assets-list :categories='@json(\App\Helpers\MarketplaceHelper::getCategories())' :show-bounty-info="{{ \App\Helpers\BountyHelper::showBountyInfo() ? 'true' : 'false' }}" search-query="{{ request()->q }}" action="{{ route('assets.fetch') }}">
            <template #no-items>
                <div class="text-2xl font-thin">No items found</div>
                <div class="mt-5 sm:mt-8 flex justify-center">
                    <div class="rounded-md shadow">
                        <a href="{{ route('assets.add') }}" class="w-full flex items-center justify-center px-4 py-3 border border-transparent text-sm font-medium rounded-md text-white bg-purple-500 hover:bg-purple-600">
                            Add New Item
                        </a>
                    </div>
                </div>
            </template>
        </assets-list>
    </div>
@endsection