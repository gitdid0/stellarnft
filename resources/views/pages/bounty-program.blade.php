@extends('layouts.app')

@section('title', __('Bounty Program').' | '.config('app.name'))

@section('content')
    <div class="container mx-auto sm:px-6 lg:px-8 py-6">
        <div class="hero-headline px-4 sm:text-center">
            <div class="sm:text-center">
                <h1 class="text-4xl tracking-tight font-thin text-purple-500 sm:text-5xl md:text-6xl">
                    <div class="block">Bounty Program</div>
                </h1>
                <p class="mt-3 text-sm text-gray-400 sm:max-w-xl sm:mx-auto">Earn up 25% by referring buyers</p>
            </div>
        </div>

        <div class="mt-14 space-y-10">
            <div>
                <div class="mt-5 sm:max-w-5xl sm:mx-auto w-full bg-white dark:bg-gray-700 shadow overflow-hidden lg:rounded-lg">
                    <div class="divide-y divide-gray-300">
                        <x-faq>
                            <x-slot name="trigger">How does the bounty program work?</x-slot>
                            <x-slot name="content">
                                <div>You can earn up to 25% of the total sale price by referring buyers. Example: Your referral buys an NFT with a bounty 10% for 1000 XLM and you receive 100 XLM (10% of the total sale price) as a <a href="https://developers.stellar.org/docs/glossary/claimable-balance/" class="text-purple-500" target="new">claimable balance</a>. You will have 7 days after the sale to claim your bounty. If you fail to <a href="{{ route('account.claimable-balances-redirect') }}" class="text-purple-500" target="new">claim your bounty</a> then the seller can claim it.</div>
                                <div class="mt-2"><span class="font-medium">Attention!</span> To receive bounty reward the selling price of an item must be at least {{ config('site.rewardsBaseReserve') }} * {{ config('site.baseReserve') }} <a href="https://developers.stellar.org/docs/glossary/minimum-balance/" class="text-purple-500" target="new">base reserve</a> = {{ config('site.rewardsBaseReserve') * config('site.baseReserve') }} XLM.</div>
                            </x-slot>
                        </x-faq>
                        <x-faq>
                            <x-slot name="trigger">How do I earn bounties?</x-slot>
                            <x-slot name="content">
                                <div>First, go to <a href="{{ route('settings.general') }}" class="text-purple-500" target="new">general settings</a> and check "Show Bounty Program Information" and press "Update" button. Then, find an NFT to refer.</div>
                                <div class="mt-2">Once you've selected an NFT that you believe you can find a buyer for, navigate to its page. You will see "Bounty Information" with your referral link.</div>
                                <div class="mt-2">When a buyer enters StellarNFT through your referral link, StellarNFT will automatically detect it and for the next 30 days you will receive a bounty on every item the buyer purchases.</div>
                            </x-slot>
                        </x-faq>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection