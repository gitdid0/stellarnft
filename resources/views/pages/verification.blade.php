@extends('layouts.app')

@section('title', __('Verification Form').' | '.config('app.name'))

@section('content')
    <div class="container mx-auto sm:px-6 lg:px-8 py-6">
        <div class="hero-headline px-4 sm:text-center">
            <div class="sm:text-center">
                <h1 class="text-4xl tracking-tight font-thin text-purple-500 sm:text-5xl md:text-6xl">
                    <div class="block">Verification Form</div>
                </h1>
                <p class="mt-3 text-sm text-gray-400 sm:max-w-xl sm:mx-auto">Let's get you that sweet "verified" badge</p>
            </div>
        </div>

        <div class="mt-14 flex flex-col sm:justify-center items-center">
            <div class="w-full sm:max-w-md px-6 py-4 bg-white dark:bg-gray-700 shadow overflow-hidden sm:rounded-lg">

                <form method="post" action="{{ route('verification.submit') }}">
                    @csrf

                    <div class="">
                        <x-label for="name" :value="__('Name')" />
                        <x-input id="name" class="block mt-1 w-full" type="text" :value="old('name')" name="name" required />
                        @error('name')
                            <x-validation-error :message="$message" />
                        @enderror
                    </div>

                    <div class="mt-4">
                        <x-label for="screenshot" :value="__('Screenshot')" />
                        <x-input id="screenshot" class="block mt-1 w-full" type="text" :value="old('screenshot')" name="screenshot" required />
                        @error('screenshot')
                            <x-validation-error :message="$message" />
                        @enderror
                        <p class="mt-2 text-sm text-gray-500">Link to a screenshot of your work in progress on one of your listed items in the editor of your choice (e.g. Photoshop, Illustrator, etc), or another backstage process.</p>
                    </div>

                    <div class="mt-4">
                        <x-label for="information" :value="__('Information')" />
                        <textarea id="information" name="information" class="rounded-md shadow-sm border-gray-300 dark:border-gray-400 focus:border-purple-500 focus:ring focus:ring-purple-500 focus:ring-opacity-50 dark:bg-gray-800 block mt-1 w-full">{{ old('information') }}</textarea>
                        @error('information')
                            <x-validation-error :message="$message" />
                        @enderror
                        <p class="mt-2 text-sm text-gray-500">Tell us about yourself. What is the concept behind items you are creating/collecting/selling?</p>
                    </div>

                    <div class="flex items-center justify-end mt-4">

                        <x-button class="ml-3">
                            {{ __('Submit Request') }}
                        </x-button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection