@extends('layouts.app')

@section('title', __('FAQ').' | '.config('app.name'))

@section('content')
    <div class="container mx-auto sm:px-6 lg:px-8 py-6">
        <div class="hero-headline px-4 sm:text-center">
            <div class="sm:text-center">
                <h1 class="text-4xl tracking-tight font-thin text-purple-500 sm:text-5xl md:text-6xl">
                    <div class="block">FAQ</div>
                </h1>
                <p class="mt-3 text-sm text-gray-400 sm:max-w-xl sm:mx-auto">Frequently Asked Questions</p>
            </div>
        </div>

        <div class="mt-14 space-y-10">
            <div>
                <div class="px-4 sm:text-center font-thin text-2xl text-purple-500">General Information</div>
                <div class="mt-5 sm:max-w-5xl sm:mx-auto w-full bg-white dark:bg-gray-700 shadow overflow-hidden lg:rounded-lg">
                    <div class="divide-y divide-gray-300">
                        <x-faq>
                            <x-slot name="trigger">What is StellarNFT?</x-slot>
                            <x-slot name="content">
                                <div>StellarNFT is digital marketplace for non-fungible tokens (NFTs) on Stellar blockchain.</div>
                            </x-slot>
                        </x-faq>
                        <x-faq>
                            <x-slot name="trigger">What is NFT (Non-fungible token)?</x-slot>
                            <x-slot name="content">
                                <div>Non-fungible tokens (NFTs) are cryptographically-generated tokens that represent something unique.</div>
                            </x-slot>
                        </x-faq>
                        <x-faq>
                            <x-slot name="trigger">Why Stellar Blockchain?</x-slot>
                            <x-slot name="content">
                                <div>Thanks to the unique consensus protocol, Stellar blockchain is one of the fastest and has practically no fees, which makes Stellar blockchain perfect for NFTs.</div>
                            </x-slot>
                        </x-faq>
                        <x-faq>
                            <x-slot name="trigger">Is StellarNFT decentralized?</x-slot>
                            <x-slot name="content">
                                <div>Yes, we utilize Stellar blockchain atomicity (if one operation fails, all operation in the transaction fails) to handle offers, so you are the only one in control of your NFTs and founds.</div>
                            </x-slot>
                        </x-faq>
                        <x-faq>
                            <x-slot name="trigger">Is StellarNFT free?</x-slot>
                            <x-slot name="content">
                                <div>We require an activation fee (1 XLM) for every new item, but you can get it back if you decide to delete the item. We don't charge buying fees/selling fees.</div>
                            </x-slot>
                        </x-faq>
                    </div>
                </div>
            </div>
        </div>

        <div class="mt-14 space-y-10">
            <div>
                <div class="px-4 sm:text-center font-thin text-2xl text-purple-500">Verification</div>
                <div class="mt-5 sm:max-w-5xl sm:mx-auto w-full bg-white dark:bg-gray-700 shadow overflow-hidden lg:rounded-lg">
                    <div class="divide-y divide-gray-300">
                        <x-faq>
                            <x-slot name="trigger">What is verification?</x-slot>
                            <x-slot name="content">
                                <div>Verified badges are granted to creators that show <span class="font-medium">enough</span> proof of authenticity and active dedication to the marketplace. NFTs created by verified creators automatically become verified, that doesn't mean that the items are 100% authentic.</div>
                            </x-slot>
                        </x-faq>
                        <x-faq>
                            <x-slot name="trigger">How to get a "verified" badge? </x-slot>
                            <x-slot name="content">
                                <div>Just list your NFTs on the marketplace and eventually, you will get your badge. If you want to receive a "verified" badge right away, fill out <a class="text-purple-500" href="{{ route('pages.verification') }}">this form</a>.</div>
                            </x-slot>
                        </x-faq>
                    </div>
                </div>
            </div>
        </div>

        <div class="mt-14 space-y-10">
            <div>
                <div class="px-4 sm:text-center font-thin text-2xl text-purple-500">Royalty System</div>
                <div class="mt-5 sm:max-w-5xl sm:mx-auto w-full bg-white dark:bg-gray-700 shadow overflow-hidden lg:rounded-lg">
                    <div class="divide-y divide-gray-300">
                        <x-faq>
                            <x-slot name="trigger">How does the royalty system work?</x-slot>
                            <x-slot name="content">
                                <div>As the original creator of an NFT, you can set a certain percentage as royalty for secondary sales. Example: You create an NFT with a royalty of 10% then sell it. Your buyer then sells your NFT for 1000 XLM and you receive 100 XLM (10% the total sale price) as a <a href="https://developers.stellar.org/docs/glossary/claimable-balance/" class="text-purple-500" target="new">claimable balance</a>. You will have 30 days after the sale to claim your revenue. If you fail to <a href="{{ route('account.claimable-balances-redirect') }}" class="text-purple-500" target="new">claim your revenue</a> then the seller (your buyer in the example) can claim it.</div>
                                <div class="mt-2"><span class="font-medium">Attention!</span> To receive royalty revenue the selling price of an item must be at least {{ config('site.rewardsBaseReserve') }} * {{ config('site.baseReserve') }} <a href="https://developers.stellar.org/docs/glossary/minimum-balance/" class="text-purple-500" target="new">base reserve</a> = {{ config('site.rewardsBaseReserve') * config('site.baseReserve') }} XLM.</div>
                            </x-slot>
                        </x-faq>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection