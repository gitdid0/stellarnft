@extends('layouts.app')

@section('title', __('Contact Form').' | '.config('app.name'))

@section('content')
    <div class="container mx-auto sm:px-6 lg:px-8 py-6">
        <div class="hero-headline px-4 sm:text-center">
            <div class="sm:text-center">
                <h1 class="text-4xl tracking-tight font-thin text-purple-500 sm:text-5xl md:text-6xl">
                    <div class="block">Contact Form</div>
                </h1>
                <p class="mt-3 text-sm text-gray-400 sm:max-w-xl sm:mx-auto">Ask and we will answer...</p>
            </div>
        </div>

        <div class="mt-14 flex flex-col sm:justify-center items-center">
            <div class="w-full sm:max-w-md px-6 py-4 bg-white dark:bg-gray-700 shadow overflow-hidden sm:rounded-lg">

                <form method="post" action="{{ route('contact.submit') }}">
                    @csrf

                    <div class="">
                        <x-label for="name" :value="__('Name')" />
                        <x-input id="name" class="block mt-1 w-full" type="text" :value="old('name')" name="name" required />
                        @error('name')
                            <x-validation-error :message="$message" />
                        @enderror
                    </div>

                    <div  class="mt-4">
                        <x-label for="email" :value="__('Email')" />
                        <x-input id="email" class="block mt-1 w-full" type="email" :value="old('email')" name="email" required />
                        @error('email')
                            <x-validation-error :message="$message" />
                        @enderror
                    </div>

                    <div class="mt-4">
                        <x-label for="message" :value="__('Message')" />
                        <textarea id="message" name="message" class="rounded-md shadow-sm border-gray-300 dark:border-gray-400 focus:border-purple-500 focus:ring focus:ring-purple-500 focus:ring-opacity-50 dark:bg-gray-800 block mt-1 w-full">{{ old('message') }}</textarea>
                        @error('message')
                            <x-validation-error :message="$message" />
                        @enderror
                    </div>

                    <input type="hidden" name="encryptedNumber" value="{{ $encryptedNumber }}">
                    <input type="hidden" name="botTest" value="{{ $number }}">

                    @error('botTest')
                    <div class="mt-4">
                        <x-validation-error :message="$message" />
                    </div>
                    @enderror

                    <div class="flex items-center justify-end mt-4">

                        <x-button class="ml-3">
                            {{ __('Send Message') }}
                        </x-button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection