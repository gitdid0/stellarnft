@extends('layouts.app')

@section('title', __('Help').' | '.config('app.name'))

@section('content')
    <div class="container mx-auto sm:px-6 lg:px-8 py-6">
        <div class="hero-headline px-4 sm:text-center">
            <div class="sm:text-center">
                <h1 class="text-4xl tracking-tight font-thin text-purple-500 sm:text-5xl md:text-6xl">
                    <div class="block">Help</div>
                </h1>
                <p class="mt-3 text-sm text-gray-400 sm:max-w-xl sm:mx-auto">Getting Started With StellarNFT</p>
            </div>
        </div>

        <div class="mt-14 space-y-10">
            <div>
                <div class="px-4 sm:text-center font-thin text-2xl text-purple-500">General Information</div>
                <div class="mt-5 sm:max-w-5xl sm:mx-auto w-full bg-white dark:bg-gray-700 shadow overflow-hidden lg:rounded-lg">
                    <div class="divide-y divide-gray-300">
                        <x-faq>
                            <x-slot name="trigger">Albedo account</x-slot>
                            <x-slot name="content">
                                <div class="space-y-6">
                                    <div>
                                        <div>Go to <a class="text-purple-500" target="help-new" href="https://albedo.link/">the Albedo website</a> then press "Create Albedo account".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/albedo/step1.png') }}" src="{{ asset('images/help/albedo/step1.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Read the information then press "Proceed".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/albedo/step2.png') }}" src="{{ asset('images/help/albedo/step2.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Enter "Password" and "Password confirmation" then press "Create account".</div>
                                        <div class="text-sm text-gray-400">You will need that password to sign transactions, so store it somewhere safe.</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/albedo/step3.png') }}" src="{{ asset('images/help/albedo/step3.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Write down the passphrase then press "I saved recovery phrase".</div>
                                        <div class="text-sm text-gray-400">Never share the passphrase, or you may lose funds.</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/albedo/step4.png') }}" src="{{ asset('images/help/albedo/step4.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Everything is set, now you can send Stellar Lumens (XLM) to your address.</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/albedo/step5.png') }}" src="{{ asset('images/help/albedo/step5.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                </div>
                            </x-slot>
                        </x-faq>
                        <x-faq>
                            <x-slot name="trigger">Login</x-slot>
                            <x-slot name="content">
                                <div class="space-y-6">
                                    <div>
                                        <div>Go to <a class="text-purple-500" target="help-new" href="{{ route('login') }}">the log in page</a> then press "Connect Wallet".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/login/step1.png') }}" src="{{ asset('images/help/login/step1.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Select your account (if you have multiple accounts) then press "Confirm using ..." (Default Account in my case).</div>
                                        <div class="text-sm text-gray-400">This action allows StellarNFT to view your public key.</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/login/step2.png') }}" src="{{ asset('images/help/login/step2.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Enter your password then press "Confirm".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/login/step3.png') }}" src="{{ asset('images/help/login/step3.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Confirm your public key (press "Reconnect" to select a different address) then press "Sign In".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/login/step4.png') }}" src="{{ asset('images/help/login/step4.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Select your account (if you have multiple accounts) then press "Confirm using ..." (Default Account in my case).</div>
                                        <div class="text-sm text-gray-400">This allows StellarNFT to verify ownership of the address. We don't submit the transaction to the blockchain (it would fail anyway because of an incorrect account sequence).</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/login/step5.png') }}" src="{{ asset('images/help/login/step5.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Enter your password then press "Confirm".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/login/step6.png') }}" src="{{ asset('images/help/login/step6.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>You have successfully logged in.</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/login/step7.png') }}" src="{{ asset('images/help/login/step7.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                </div>
                            </x-slot>
                        </x-faq>

                    </div>
                </div>
            </div>
            <div>
                <div class="px-4 sm:text-center font-thin text-2xl text-purple-500">Buying / Selling NFTs</div>
                <div class="mt-5 sm:max-w-5xl sm:mx-auto w-full bg-white dark:bg-gray-700 shadow overflow-hidden lg:rounded-lg">
                    <div class="divide-y divide-gray-300">
                        <x-faq>
                            <x-slot name="trigger">Create New NFT</x-slot>
                            <x-slot name="content">
                                <div class="space-y-6">
                                    <div>
                                        <div><a class="text-purple-500" target="help-new" href="{{ route('login') }}">Login</a> then press "Create New NFT".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/create-nft/step1.png') }}" src="{{ asset('images/help/create-nft/step1.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Fill the form then press "Next".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/create-nft/step2.png') }}" src="{{ asset('images/help/create-nft/step2.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Press "Sign & Create".</div>
                                        <div class="text-sm text-gray-400">The cost can vary based on the data you wish to store on the blockchain.</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/create-nft/step3.png') }}" src="{{ asset('images/help/create-nft/step3.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Select your account (if you have multiple accounts) then press "Confirm using ..." (Default Account in my case).</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/create-nft/step4.png') }}" src="{{ asset('images/help/create-nft/step4.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Enter your password then press "Confirm".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/create-nft/step5.png') }}" src="{{ asset('images/help/create-nft/step5.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Press "Proceed with partially signed tx".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/create-nft/step6.png') }}" src="{{ asset('images/help/create-nft/step6.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Your NFT successfully created! Now you can add new item.</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/create-nft/step7.png') }}" src="{{ asset('images/help/create-nft/step7.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                </div>
                            </x-slot>
                        </x-faq>
                        <x-faq>
                            <x-slot name="trigger">Add New Item</x-slot>
                            <x-slot name="content">
                                <div class="space-y-6">
                                    <div>
                                        <div><a class="text-purple-500" target="help-new" href="{{ route('login') }}">Login</a> then press "Create New NFT".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/add-item/step1.png') }}" src="{{ asset('images/help/add-item/step1.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Fill the form then press "Add".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/add-item/step2.png') }}" src="{{ asset('images/help/add-item/step2.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>The item has been crated. Now you have to activate it. Press "Activate".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/add-item/step3.png') }}" src="{{ asset('images/help/add-item/step3.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Press "Sign & Activate".</div>
                                        <div class="text-sm text-gray-400">You can get the activation cost back if you decide to delete the item.</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/add-item/step4.png') }}" src="{{ asset('images/help/add-item/step4.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Select your account (if you have multiple accounts) then press "Confirm using ..." (Default Account in my case).</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/add-item/step5.png') }}" src="{{ asset('images/help/add-item/step5.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Enter your password then press "Confirm".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/add-item/step6.png') }}" src="{{ asset('images/help/add-item/step6.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>The item now activated and you can receive buy offers.</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/add-item/step7.png') }}" src="{{ asset('images/help/add-item/step7.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/add-item/step7-1.png') }}" src="{{ asset('images/help/add-item/step7-1.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                </div>
                            </x-slot>
                        </x-faq>

                        <x-faq>
                            <x-slot name="trigger">Make Offer</x-slot>
                            <x-slot name="content">
                                <div class="space-y-6">
                                    <div>
                                        <div><a class="text-purple-500" target="help-new" href="{{ route('login') }}">Login</a> then press "Make Offer".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/make-offer/step1.png') }}" src="{{ asset('images/help/make-offer/step1.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Enter the price then press "Next".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/make-offer/step2.png') }}" src="{{ asset('images/help/make-offer/step2.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Press "Sign & Make Offer".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/make-offer/step3.png') }}" src="{{ asset('images/help/make-offer/step3.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Select your account (if you have multiple accounts) then press "Confirm using ..." (Account 1 in my case).</div>
                                        <div class="text-sm text-gray-400">StellarNFT utilizes Stellar blockchain atomicity (if one operation fails, all operation in the transaction fails) to handle offers, so you don't pay anything unless you receive your NFT.</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/make-offer/step4.png') }}" src="{{ asset('images/help/make-offer/step4.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Enter your password then press "Confirm".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/make-offer/step5.png') }}" src="{{ asset('images/help/make-offer/step5.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Press "Proceed with partially signed tx".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/make-offer/step6.png') }}" src="{{ asset('images/help/make-offer/step6.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Your offer has been submitted.</div>
                                    </div>
                                </div>
                            </x-slot>
                        </x-faq>

                        <x-faq>
                            <x-slot name="trigger">Accept Offer</x-slot>
                            <x-slot name="content">
                                <div class="space-y-6">
                                    <div>
                                        <div><a class="text-purple-500" target="help-new" href="{{ route('login') }}">Login</a> then press "Accept".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/accept-offer/step1.png') }}" src="{{ asset('images/help/accept-offer/step1.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Press "Sign & Accept Offer".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/accept-offer/step2.png') }}" src="{{ asset('images/help/accept-offer/step2.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Select your account (if you have multiple accounts) then press "Confirm using ..." (Default Account in my case).</div>
                                        <div class="text-sm text-gray-400">StellarNFT utilizes Stellar blockchain atomicity (if one operation fails, all operation in the transaction fails) to handle offers, so you don't send your NFT anywhere unless you receive payment.</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/accept-offer/step3.png') }}" src="{{ asset('images/help/accept-offer/step3.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Enter your password then press "Confirm".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/accept-offer/step4.png') }}" src="{{ asset('images/help/accept-offer/step4.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Press "Proceed with partially signed tx".</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/accept-offer/step5.png') }}" src="{{ asset('images/help/accept-offer/step5.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div>Congratulation! You made a sell!</div>
                                        <div class="mt-2">
                                            <v-img full-url="{{ asset('images/help/accept-offer/step6.png') }}" src="{{ asset('images/help/accept-offer/step6.png') }}" img-class="w-48 h-auto border border-gray-400"></v-img>
                                        </div>
                                    </div>
                                </div>
                            </x-slot>
                        </x-faq>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection