@extends('layouts.app')

@section('styles')
<link href="{{ mix('css/error.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="py-12 px-4 sm:text-center">
	<div class="error sm:mx-auto" data-text="@yield('code')">@yield('code')</div>
	<p class="text-gray-800 dark:text-gray-400">@yield('message')</p>
</div>
@endsection