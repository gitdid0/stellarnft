@props([])

@php

$openClass = 'block font-thin text-purple-500 cursor-pointer';
$unOpenClass = 'block font-thin hover:text-purple-500';

@endphp

<dropdown v-slot="{ open, toggle, hide }">
    <div class="p-4">
        <a href="javascript:;" @click="toggle" :class="open ? '{{ $openClass }}' : '{{ $unOpenClass }}'">{{ $trigger }}</a>
        <div class="mt-5" v-if="open">{{ $content }}</div>
    </div>
</dropdown>
