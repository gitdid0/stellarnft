@props(['message'])

@if ($message)
    <span {{ $attributes->merge(['class' => 'text-sm text-red-600']) }} role="alert">{{ $message }}</strong></span>
@endif
