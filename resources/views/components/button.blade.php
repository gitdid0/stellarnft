@php

$element = 'button';

if(!empty($href)) $element = 'a';

@endphp

<{{ $element }} {{ $attributes->merge(['type' => 'submit', 'class' => 'inline-flex items-center text-sm font-medium leading-6 bg-gray-100 dark:bg-gray-800 py-2 px-3 rounded text-purple-500 hover:bg-gray-200 dark:hover:bg-gray-600 transition duration-150 ease-in-out focus:outline-none disabled:opacity-25 uppercase']) }}>
    {{ $slot }}
</{{ $element }}>