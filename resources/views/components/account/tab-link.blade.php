@props(['active'])

@php
$classes = ($active ?? false)
            ? 'tab-active mx-2 inline-flex items-center justify-center p-2 border border-transparent text-base font-medium rounded-md text-purple-700 bg-purple-100'
            : 'inline-flex mx-2 items-center justify-center p-2 border border-transparent text-base font-medium rounded-md hover:text-purple-700 hover:bg-purple-100';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
