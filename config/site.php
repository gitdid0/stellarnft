<?php

return [

    'site_wallet_address' => env('SITE_WALLET_ADDRESS', 'GDX3AVBSYXGMYHROF6I7D22TX6YGBU472FJI7YIK4SACV4JS26CAUGNW'),
    'site_wallet_secret' => env('SITE_WALLET_ADDRESS_SECRET'),
    'baseReserve' => 0.5,
    'slack_hook' => env('SITE_SLACK_HOOK', false),

    'horizonServer' => env('HORIZON_SERVER', 'https://horizon-testnet.stellar.org/'),

    'tmp_folder' => env('TMP_FOLDER_PATH', ''),

    'stroop' => '0.0000001',

    'priceRatio' => 10000000,

    'royaltyDeadline' => 2592000,

    'bountyDeadline' => 604800,

    'maxRoyaltyReward' => 0.25,

    'maxBountyReward' => 0.25,

    'rewardsBaseReserve' => 10,

    'static_files_host' => env('STATIC_FILES_HOST', ''),

    'useCloudStorage' => env('USE_CLOUD_STORAGE', false),

    'administrators' => [
        env('ADMIN_STELLAR_ADDRESS', 'GBY36CUHELCC42J2JAIWUBUZEJK4OBOQSP6OOC7O2VH334E4NX7MX5RV')
    ],
    
    'wallets' => [
        'walletconnect' => [
            'name' => 'WalletConnect',
            'icon' => '/images/walletconnect.svg',
            'link' => 'https://lobstr.co/',
            'driver' => 'driver-walletconnect'
        ],
        'albedo' => [
            'name' => 'Albedo',
            'icon' => '/images/albedo-icon.svg',
            'link' => 'https://albedo.link/',
            'driver' => 'driver-default'
        ],
        'rabet' => [
            'name' => 'Rabet',
            'icon' => '/images/rabet.svg',
            'link' => 'https://rabet.io/',
            'driver' => 'driver-default'
        ],
        'freighter' => [
            'name' => 'Freighter',
            'icon' => '/images/freighter-icon.png',
            'link' => 'https://www.freighter.app/',
            'driver' => 'driver-default'
        ],
    ]
];
