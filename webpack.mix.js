const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

const fsExtra = require('fs-extra')

mix.before(() => {
    fsExtra.emptyDirSync('public/js')
});


mix.webpackConfig({
	output: {
        chunkFilename: (pathData) => {
            let vendors = ['stellar', 'filepond'];
            return vendors.includes(pathData.chunk.name) ? 'js/[name].js' : "js/[name].[chunkhash].js";
        }
    }
});
 
mix.js('resources/js/app.js', 'public/js').vue()

mix.js('resources/js/wallet-helper.js', 'public/js')

mix.js('resources/js/plyr.js', 'public/js')

const tailwindcss = require('tailwindcss')
const autoprefixer = require('autoprefixer')

mix.sass('resources/css/error.scss', 'public/css')

mix.sass('resources/css/toast.scss', 'public/css')

mix.sass('resources/css/plyr.scss', 'public/css')

if (mix.inProduction()) {
    console.log('production')
    mix.sass('resources/css/app.scss', 'public/css', {}, [ 
        tailwindcss('tailwind.config.js'),
        autoprefixer({
            grid: true
        })
    ])
    mix.sass('resources/css/dashboard.scss', 'public/css', {}, [ 
        tailwindcss('tailwind-dashboard.config.js'),
        autoprefixer({
            grid: true
        })
    ])
    mix.version()
} else {
    console.log('local')
    mix.sass('resources/css/app.scss', 'public/css', {}, [ 
        tailwindcss('tailwind.config.js')
    ])
    mix.sass('resources/css/dashboard.scss', 'public/css', {}, [ 
        tailwindcss('tailwind-dashboard.config.js')
    ])
    mix.version()
}

mix.options({
    processCssUrls: false
})